$(document).ready(function(){

    FastClick.attach(document.body);


    /** Animations **/
    // var $dataAnimateEl = $('[data-animate]');
    // if( $dataAnimateEl.length > 0 ){
    //
    //     $dataAnimateEl.each(function(){
    //         var element = $(this),
    //             animationOut = element.attr('data-animate-out'),
    //             animationDelay = element.attr('data-delay'),
    //             animationDelayOut = element.attr('data-delay-out'),
    //             animationDelayTime = 0,
    //             animationDelayOutTime = 3000;
    //
    //         if( element.parents('.fslider.no-thumbs-animate').length > 0 ) { return true; }
    //
    //         if( animationDelay ) { animationDelayTime = Number( animationDelay ) + 500; } else { animationDelayTime = 500; }
    //         if( animationOut && animationDelayOut ) { animationDelayOutTime = Number( animationDelayOut ) + animationDelayTime; }
    //
    //         if( !element.hasClass('animated') ) {
    //             element.addClass('not-animated');
    //             var elementAnimation = element.attr('data-animate');
    //             element.appear(function () {
    //                 setTimeout(function() {
    //                     element.removeClass('not-animated').addClass( elementAnimation + ' animated');
    //                 }, animationDelayTime);
    //
    //                 if( animationOut ) {
    //                     setTimeout( function() {
    //                         element.removeClass( elementAnimation ).addClass( animationOut );
    //                     }, animationDelayOutTime );
    //                 }
    //             },{accX: 0, accY: -120},'easeInCubic');
    //         }
    //     });
    //
    // }

    /** Animations **/


    $('body').on('click', function (e) {
        //did not click a popover toggle or popover
        if ($(e.target).data('toggle') !== 'popover'
            && $(e.target).parents('.popover.in').length === 0) {
            $('[data-toggle="popover"]').popover('hide');
        }
    });



    $(document).on('click','.close-header-popup',function(){
        // console.log("clicked on "+$(this).parent().parent());
        animateCloseHeaderPopup($(this).parent().parent())
    });



    /*
    Body loader
     */
    $('.body-loader').addClass('loaded');
    setTimeout(function(){
        $('.body-loader').css('display','none');
    },500);


    if(jQuery.Lazy){
        $('.lazy').Lazy();
    }



    $(window).scroll(function (event) {
        var scroll = $(window).scrollTop();
        if (scroll > 100) {
            $('#headerNavigation').addClass('condensed');
        } else {
            $('#headerNavigation').removeClass('condensed');
        }
    });

    $('#navigation-toggle').click(function (e) {
        e.preventDefault();
        if ($('#header-navigation').hasClass('active')) {
            $('#header-navigation').removeClass('active');
            $('body').removeClass('noscroll');
        } else {
            $('#header-navigation').addClass('active');
            $('body').addClass('noscroll');
        }

    });


    $('select').not('.searchBox .simpleSelect .darkSelect').select2({
        'width': '100%',
        minimumResultsForSearch: Infinity,
        templateResult: function(result, container) {
            if (!result.id) {
                return result.text;
            }
            container.className += ' needsclick';
            return result.text;
        }
    }).each(function(index, el){
        $(el).data('select2').$container.find('*').addClass('needsclick');
    });

    $('select.searchBox').not('.simpleSelect .darkSelect').select2({
        'width': '100%',
        minimumResultsForSearch: 2,

        templateResult: function(result, container) {
            if (!result.id) {
                return result.text;
            }
            container.className += ' needsclick';
            return result.text;
        }
    }).on("select2:open", function () {
        $('.select2-results__options').niceScroll();
    }).each(function(index, el){
        $(el).data('select2').$container.find('*').addClass('needsclick');
    });

    $('select.darkSelect').not('.searchBox').select2({
        'width': '100%',
        containerCssClass: 'darkSelect',
        dropdownCssClass: 'darkSelect',
        minimumResultsForSearch: Infinity,
        templateResult: function(result, container) {
            if (!result.id) {
                return result.text;
            }
            container.className += ' needsclick';
            return result.text;
        }
    }).each(function(index, el){
        $(el).data('select2').$container.find('*').addClass('needsclick');
    });

    $('select.darkSelect.searchBox').select2({
        'width': '100%',
        minimumResultsForSearch: 2,
        containerCssClass: 'darkSelect',
        dropdownCssClass: 'darkSelect',
        templateResult: function(result, container) {
            if (!result.id) {
                return result.text;
            }
            container.className += ' needsclick';
            return result.text;
        }
    }).on("select2:open", function () {
        $('.select2-results__options').niceScroll();
    }).each(function(index, el){
        $(el).data('select2').$container.find('*').addClass('needsclick');
    });

    $('.select2 span').addClass('needsclick');
    $('.select2-results__option').addClass('needsclick');


    if ($('#filterForm').length) {
        $('#filterForm').submit(function(e) {
            e.preventDefault();
            sendFilterForm($('select[name=sort]').val());
        });
    }

    if ( $('form#contactForm').length) {
        $('form#contactForm').submit(function(e) {
            e.preventDefault();

            animateClosePopups();
            loading_start();

            let values = $(this).serialize();

            $.ajax({
                url: '/ajax/contact',
                type: 'POST',
                data: values,
                dataType: 'json',
                success: function(result) {

                    if(result.status == 'fail') {

                        if(result.token) {
                            $('#cfCSRF').attr('name', result.tokenKey);
                            $('#cfCSRF').attr('value', result.token);
                        }
                        //parse errors and run error messages
                        loading_stop();

                        var captchaReset = result.isCaptchaReset !== 'undefined' ? result.isCaptchaReset : true;
                        contactFormCaptcha.reset(captchaReset);

                        if(result.url) {
                            window.location = result.url;
                        }
                        // if (result.isReload) {
                        //     location.reload();
                        // }

                        //reset error class
                        $(".form-group").each(function(index) {
                            $(this).removeClass('has-error');
                        });
                        if(Object.keys(result.messages).length) {
                            if(Object.keys(result.messages).length > 1) {
                                let alerts_html = '<ul>';
                                $.each(result.messages,function(key,val){
                                    $('#'+key).parent().addClass('has-error');
                                    alerts_html += '<li>' + val + '</li>';
                                });
                                alerts_html += '</ul>';
                                animateOpenHeaderPopup('error', alerts_html);
                            } else {
                                $.each(result.messages,function(key,val){
                                    animateOpenHeaderPopup('error', val);
                                    $('#'+key).parent().addClass('has-error');
                                });
                            }
                        }
                    } else {
                        loading_stop();
                        animateOpenHeaderPopup('success', result.message);
                        //animateOpenHeaderPopup('success', result.message, '#pfAlertBox');
                        $('form#contactForm button').attr("disabled", true);
                    }

                },
                fail: function() {
                    animateOpenHeaderPopup('error',checkInternet);
                }
            });

        });
    }

    if ($('#headerCarousel').length && $("#headerCarousel").is(":hidden") && !$("#showFilterBtn").hasClass('p-t-5')) {
        $("#showFilterBtn").addClass('p-t-5');
    }
});

function sendFilterForm(sort='') {

    //let serializedData = $("#filterForm").serialize().replace(/&?[^=&]+=(&|$)/g,'');
    let data = {fs:1};
    $.each($('#filterForm').serializeArray(), function(i, field) {
        if (field.value && field.value != '' && field.value != ' ' && field.value != '0') {
            data[field.name] = field.value;
        }
    });
    if (sort !=='') {
        data['sort'] = sort;
    }
    let serializedData = $.param(data)
    if (serializedData) {
        //let url = url + '?' + args.join('&');
        // animateClosePopups();
        window.location.href = '/index.php?'+serializedData;
        // } else  {
        //     animateOpenHeaderPopup('error', "<div class='errorMessage'>"+$('#filterForm').attr('data-errormsg-empty-form')+"</div>");
    }
}

function animateCloseHeaderPopup($el) {
    // console.log("animate to close for "+$el.toString());
    $('.header-popup-container',$el).animate({
        opacity:0,
    },300,function(){
        $el.animate({
            height:0
        },300,function () {
            $el.remove()
        })
    });
}

function animateOpenHeaderPopup(popupType,popupText,elementId) {
    let headerPopup = '<div class="header-popup '+popupType+'"><div class="header-popup-container">';
    headerPopup += popupText;
    headerPopup += '</div><div class="header-popup-btn"><i class="close-header-popup ti-close pull-right m-t-3"></i></div></div>';
    let $headerPopup = $(headerPopup);

    if (elementId === undefined) {
        $('#errorBox').append($headerPopup);
    } else {
        $(elementId).append($headerPopup);
    }
}

function animateClosePopups() {
    $('.header-popup').animate({
        opacity:0,
    },300,function(){
        $(this).animate({
            height:0
        },300,function () {
            $(this).remove()
        })
    });
}

function loading_start(){
    var window_height = $( window ).height();
    var document_height = $( document ).height();
    var top_offset = $( window ).scrollTop();
    $("body").append('<div class="loaderOverlay"><div class="cs-loader"><div class="cs-loader-inner"><label>●</label><label>●</label><label>●</label><label>●</label><label>●</label><label>●</label></div></div></div>');
    $(".loaderOverlay").height(document_height);
    $(".loaderOverlay .cs-loader").css('top', top_offset + (window_height/2 - 25) + 'px');
}
function loading_stop(){
    $('body .loaderOverlay').remove();
}

function filterTowns(regions) {
    let regionId = $('#region option:selected').val();
    if (typeof townsList !== 'undefined' && typeof townsList === 'object') {
        $('#town').empty();
        $.each(townsList, function(key, val) {
            if (!regionId || (val.id == 0 && val.region_id == 0) || (regionId && regionId == val.region_id)) {
                $('#town').append(new Option(val.name, val.id, false, false));
            }
        });
        $('#town').trigger('change');
    }
}

function hideFilter(isHeaderImages) {
    if (isHeaderImages) {
        $("#formBox").parent().removeClass('p-t-20').addClass('p-t-5');
    }
    $("#formBox").slideToggle('fast');
    $("#hideFilterBtn").hide();
    $("#showFilterBtn").css('display', 'flex');
}

function showFilter(isHeaderImages) {
    if (isHeaderImages) {
        $("#formBox").parent().removeClass('p-t-5').addClass('p-t-20');
    }
    $("#formBox").slideToggle('fast');
    $("#showFilterBtn").hide();
    $("#hideFilterBtn").show();
}


/* Google CAPTCHA */
var GreCaptcha = {
    formId: '',
    action: '',
    inputSiteKeyName: 'googleReCaptchaSiteKey',
    containerId: 'g-recaptcha',
    widgetId: undefined,

    initV2: function () {
        this.widgetId = grecaptcha.render(this.containerId, {
            'sitekey': $("input[name=" + this.inputSiteKeyName + "]").val(),
            'theme': 'light'
        });
        $("#" + this.containerId).show();
    },
    reset: function (reset) {
        if (reset) {
            grecaptcha.reset(this.widgetId);
        }
    }
};

let contactFormCaptcha = { formId: 'contactForm', action: 'ajax_contact' };
contactFormCaptcha.__proto__ = GreCaptcha;
var contactFormCaptchaCallbackV2 = function () { contactFormCaptcha.initV2(); };