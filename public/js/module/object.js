/**
 * Created by EV 12/27/2019.
 */
$(document).ready(function(){

    $('.object-header__preview').slick({
        mobileFirst: true,
        arrows: false,
        dots: true,
        draggable: false,
        autoplay: false,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    asNavFor: '.object-header__nav',
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    fade: true,
                    dots: false,
                    arrows: true,
                    nextArrow: '<i class="slick-next ti-angle-right"></i>',
                    prevArrow: '<i class="slick-prev ti-angle-left"></i>'
                }
            }
        ]
    });

    let totalObjectImages = $('.object-header__nav').find('div').length;
    if (totalObjectImages) {
        $('.object-header__nav').slick({
            slidesToShow: (totalObjectImages <= 5 ? totalObjectImages - 1 : 5),
            slidesToScroll: 1,
            asNavFor: '.object-header__preview',
            dots: true,
            centerMode: true,
            focusOnSelect: true,
            arrows: false,
            responsive: [
                {
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: (totalObjectImages <= 5 ? totalObjectImages - 1 : 5),
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: (totalObjectImages <= 4 ? totalObjectImages - 1 : 4),
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
});

