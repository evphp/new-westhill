<?php

use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;

//use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
//use Phalcon\Mvc\Model\Manager as ModelsManager;
use Phalcon\Session\Adapter\Files as SessionAdapter;
//use Phalcon\Flash\Session as FlashSession;

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include APP_PATH . '/config/config.php';
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});


/**
 * All requests go trough security dispatcher
 */
$di->set('dispatcher', function() use ($di) {
	$eventsManager = $di->getShared('eventsManager');
	$security = new Security($di);
	$eventsManager->attach('dispatch', $security);

	// Listen all the database events ???

    $dispatcher = new Phalcon\Mvc\Dispatcher();
	$dispatcher->setEventsManager($eventsManager);

	return $dispatcher;
});


/**
 * Setting up the view component
 */
$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new Phalcon\Mvc\View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines([
        '.volt' => function ($view) {
            $config = $this->getConfig();

            $volt = new VoltEngine($view, $this);

            $di = $view->getDI();

            $volt->setOptions([
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_'
            ]);

	        $volt->getCompiler()->addFunction('tx', function ($resolvedArgs, $exprArgs) {
		        return sprintf('$this->tx->query(%s)', $resolvedArgs);
	        });

             $volt->getCompiler()->addFilter('tx', function ($resolvedArgs, $exprArgs) use ($di) {
              return sprintf('$this->tx->query(%s)', $resolvedArgs);
            });

            $volt->getCompiler()->addFunction('rtx', function ($resolvedArgs, $exprArgs) {
                return sprintf('$this->tx->replace(%s)', $resolvedArgs);
            });

//	        $volt->getCompiler()->addFunction(
//		        'contains_text',
//		        function ($resolvedArgs, $exprArgs) {
//			        if (function_exists('mb_stripos')) {
//				        return 'mb_stripos(' . $resolvedArgs . ')';
//			        } else {
//				        return 'stripos(' . $resolvedArgs . ')';
//			        }
//                }
//            );

	        $volt->getCompiler()->addFilter('getAttribute', function ($resolvedArgs, $exprArgs) use ($di)
	        {
		        return vsprintf('%s->{%s}', explode(', ', $resolvedArgs));
	        });

	        $volt->getCompiler()->addFunction('in_array', 'in_array');

	        $volt->getCompiler()->addFunction('round','round');
	        $volt->getCompiler()->addFunction('roundDown',function($resolvedArgs, $exprArgs){
		        return 'round('.$resolvedArgs.',PHP_ROUND_HALF_DOWN)';
	        });
	        $volt->getCompiler()->addFunction('formatNumber',function($resolvedArgs, $exprArgs){
		        //$dotPosition  = mb_strpos($resolvedArgs,'.');
		        return 'sprintf("%01.2f", ((mb_strpos((string) '.$resolvedArgs.',"."))?mb_strcut('.$resolvedArgs.',0,mb_strpos((string) '.$resolvedArgs.',".")+3):'.$resolvedArgs.'))';
	        });
	        $volt->getCompiler()->addFunction('formatNumberZero',function($resolvedArgs, $exprArgs){
		        //$dotPosition  = mb_strpos($resolvedArgs,'.');
		        return 'number_format('.$resolvedArgs.', 1, \'.\', \'\');';
	        });

	        $volt->getCompiler()->addFunction('formatDateForCalendar',function($resolvedArgs, $exprArgs){
		        return "";
	        });

//	        $volt->getCompiler()->addFunction('countryByIso', function ($resolvedArgs, $exprArgs) use ($di) {
//		        return sprintf('$this->translate->query2(\SysCountry::findFirstByCountryIso(%s)->countryName)', $resolvedArgs);
//	        });

	        $volt->getCompiler()->addFilter('fromUnixtime', function ($resolvedArgs, $exprArgs) use ($di) {
		        return sprintf('date("d.m.Y",%s)',$resolvedArgs);
	        });
	        $volt->getCompiler()->addFilter('timeFromUnixtime', function ($resolvedArgs, $exprArgs) use ($di) {
		        return sprintf('date("H:i",%s)',$resolvedArgs);
	        });
	        $volt->getCompiler()->addFilter('yearFromUnixtime', function ($resolvedArgs, $exprArgs) use ($di) {
		        return sprintf('date("Y",%s)',$resolvedArgs);
	        });

            return $volt;
        },
        '.phtml' => PhpEngine::class

    ]);

    return $view;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
    $config = $this->getConfig();

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;

    $params = [
        'host'     => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname'   => $config->database->dbname,
        'charset'  => $config->database->charset,
        'port'     => "3306"
    ];

    $connection = new $class($params);

    return $connection;
});


/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
//$di->setShared('modelsMetadata', function () {
//    return new MetaDataAdapter();
//});

//$di->set("modelsManager", function() {
//        return new ModelsManager();
//});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
//$di->set('flash', function () {
//    return new FlashSession([
//        'error'   => 'alert alert-danger',
//        'success' => 'alert alert-success',
//        'notice'  => 'alert alert-info',
//        'warning' => 'alert alert-warning'
//    ]);
//});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});



/**
 * Encryption functionality (mostly for cookies)
 */
//$di->set('crypt', function () {
//    $crypt = new Phalcon\Crypt();
//	$crypt->setKey($this->getConfig()->application->encryptKey);
//	return $crypt;
//});



$di->setShared('tx', function() use($di) {
//	$dispatcher = $di->get('dispatcher');

	$language = $this->getConfig()->application->defaultLang;
	if($di->getShared('session')->has('lang') && $di->getShared('session')->get('lang') != ''){
		$language = $di->getShared('session')->get('lang');
	}

	return new Library\Texts\Adapter\Database([
		'db' => $di->get('db'), // Here we're getting the database from DI
		'table' => 'texts', // The table that is storing the translations
		'language' => $language, // Now we're getting the best language for the user
//		'controller' => $di->getShared('router')->getControllerName(),
//		'action' => $di->getShared('router')->getActionName()
	]);
});


$di->setShared('notify', function(){
	$notify = new Notify();
	return $notify;
});
//
//$di->setShared('security', function () {
//	$security = new \Phalcon\Security;
//	return $security;
//});