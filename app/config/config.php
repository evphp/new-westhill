<?php
/*
 * Modified: prepend directory path of current file, because of this file own different ENV under between Apache and command line.
 * NOTE: please remove this comment.
 */
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');

return new \Phalcon\Config([
	'database' => [
		'adapter'     => 'Mysql',
		'host'        => 'localhost',
		'username'    => 'westhill_me',
		'password'    => 'quoh9YoojohnahL9',
		'dbname'      => 'westhill_me',
		'charset'     => 'utf8',
	],
	'countryInfo' => array(
		'countryId' => 1
	),
    'mail' => array(
        'developerMail' => 'e.varlamov@gmail.com',
        'fromName' => 'westhill',
        'fromEmail' => 'info@westhill.me', //'webpage@westhill.me'
        'smtp' => array(
            'server' => '127.0.0.1',
            'port' => 25,
            'security' => '',
            'username' => '',
            'password' => ''
        )
    ),
	'siteInfo' => array(
	    'name' => 'WestHill Property',
		'email' => 'info@westhill.me',
		'phone' => '+382 69734057 (Viber, WhatsApp)',
		'phoneExtra' => '+382 69231168 (Viber, WhatsApp)',
        'slogan' => 'Real Estate Agency in Montenegro',
        'address' => 'Blazo Jovanovića 17, 85310 Budva, Montenegro',
        'facebook' => 'https://www.facebook.com/westhill.me/'
	),
//	'googleAuth' => array(
//		'from' => 'aaa',
//	),
    'googleRecaptcha' => [
        'url' => 'https://www.google.com/recaptcha/api/siteverify',
        'v2' => [
            'public_key' => '6LcmYNYUAAAAADbI0zo3BRWeERaZVIvYMKGz8oO_',
            'secret_key' => '6LcmYNYUAAAAAEZTopyBVprDub-foHawhC9_R8AU'
        ],
        'v3' => [
            'public_key' => '',
            'secret_key' => ''
        ]
    ],
	'sms' => array(
		'from' => '',
		'username' => '',
		'password' => '',
	),
	'application' => [
		'publicUrl' => 'http://new.westhill.loc/',
		'appDir'         => APP_PATH . '/',
		'controllersDir' => APP_PATH . '/controllers/',
		'modelsDir'      => APP_PATH . '/models/',
		'migrationsDir'  => APP_PATH . '/migrations/',
		'viewsDir'       => APP_PATH . '/views/',
		'pluginsDir'     => APP_PATH . '/plugins/',
		'libraryDir'     => APP_PATH . '/library/',
        'componentsDir'  => APP_PATH . '/components/',
//        'enumDir'        => APP_PATH . '/enum/',
		'cacheDir'       => BASE_PATH . '/cache/',
//      'certDir'        => APP_PATH . '/cert/',
//		'documentDir'    => BASE_PATH . '/documents/',
        'publicDir'      => BASE_PATH . '/public/',
        'filePrefix'     => 'https://westhill.me',
        'backendSite'    => 'https://backend.westhill.me',
        'defaultLang'    => 'en',
        'availableLanguages' => array('en','ru','cr','tr'),
        'enabledLanguages' => array(
            'ru' => array('id' => 1, 'iso_code' => 'ru', 'iso_country' => 'ru', 'short_lang' => 'Рус', 'lang' => 'Русский'),
            'en' => array('id' => 2, 'iso_code' => 'en', 'iso_country' => 'gb', 'short_lang' => 'Eng', 'lang' => 'English'),
            'cr' => array('id' => 3, 'iso_code' => 'cr', 'iso_country' => 'me', 'short_lang' => 'Cnr', 'lang' => 'црногорски језик'),
            'tr' => array('id' => 4, 'iso_code' => 'tr', 'iso_country' => 'tr', 'short_lang' => 'Tur', 'lang' => 'Türkçe'),
        ),
//		'encryptKey'     => 'KLdf$#op234uyi',
//		'allowedDocumentTypes' => array('image/jpeg','image/jpg','image/png','application/pdf','application/octet-stream','application/msword','application/vnd.oasis.opendocument.text','application/vnd.etsi.asic-e+zip','application/zip'),

		// This allows the baseUri to be understand project paths that are not in the root directory
		// of the webpspace.  This will break if the public/index.php entry point is moved or
		// possibly if the web server rewrite rules are changed. This can also be set to a static path.
		//'baseUri'        => preg_replace('/public([\/\\\\])index.php$/', '', $_SERVER["PHP_SELF"]),
        'baseUri' => '/',
	],
]);
