<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir,
        $config->application->libraryDir,
        $config->application->pluginsDir,
        $config->application->componentsDir,
        //$config->application->enumDir,
    ]
)->register();

$loader->registerNamespaces(array(
	'Library' => $config->application->libraryDir,
	'Library\Translate\Adapter' => $config->application->libraryDir.'/Translate/Adapter',
    'Components\Forms' => $config->application->componentsDir . 'Forms',
    'Components\Validators' => $config->application->componentsDir . 'Validators',
    'Components\Forms\Elements' => $config->application->componentsDir . 'Forms/Elements',
    'Models' => $config->application->modelsDir,
    //'Enum' => $config->application->enumDir,
));

//$loader->registerFiles([BASE_PATH . '/vendor/autoload.php']);

$loader->register();
