<?php

$router = $di->getRouter();

// Define your routes here
$router->add('/{language:[a-z]{2}+}/?', array(
        'controller' => 'index',
        'action' => 'index',
));

$router->add('/:controller/:action/:params', array(
        'controller' => 1,
        'action' => 2,
        'params' => 3,
));

$router->add('/{language:[a-z]{2}+}/:controller', array(
        'controller' => 2
));

//routing with underscore actions
$router->add('/:controller/([a-zA-Z]+_[a-zA-Z]+)/:params', array(
        'controller' => 1,
        'action' => 2,
        'params' => 3,
))->convert('action', function ($action) {
    return lcfirst(Phalcon\Text::camelize($action));
});

//Object hack
$router->add('/object/print/:params', array(
    'controller' => 'object',
    'action' => 'print',
    'params' => 1,
));
$router->add('/object/:action', array(
    'controller' => 'object',
    'action' => 'index',
    'params' => 1,
));

$router->add('/{language:[a-z]{2}+}/:controller/:action/:params', array(
        'controller' => 2,
        'action' => 3,
        'params' => 4,
));

//routing with underscore actions
$router->add('/{language:[a-z]{2}+}/:controller/([a-zA-Z]+_[a-zA-Z]+)/:params', array(
        'controller' => 2,
        'action' => 3,
        'params' => 4,
))->convert('action', function ($action) {
    return lcfirst(Phalcon\Text::camelize($action));
});

//Object hack
$router->add('/{language:[a-z]{2}+}/object/print/:params', array(
    'controller' => 'object',
    'action' => 'print',
    'params' => 2,
));
$router->add('/{language:[a-z]{2}+}/object/:action', array(
    'controller' => 'object',
    'action' => 'index',
    'params' => 2,
));


$router->handle();