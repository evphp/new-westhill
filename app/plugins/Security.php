<?php
/**
 * Created by PhpStorm.
 * User: evarlamov
 * Date: 07/12/91
 * Time: 13:53
 */

//namespace
use Phalcon\Events\Event,
    Phalcon\Mvc\User\Plugin,
    Phalcon\Mvc\Dispatcher;
    //Phalcon\Acl;

use Phalcon\Mvc\Dispatcher\Exception;

class Security extends Plugin
{
    public function __construct($dependencyInjector)
    {
        $this->_dependencyInjector = $dependencyInjector;
    }

    public function beforeException(Event $event, Dispatcher $dispatcher, $exception)
    {

        //
        switch ($exception->getCode()) {
            case 0:

                $dispatcher->forward(array(
                    'controller' => 'error',
                    'action' => 'pageIsNotResponding',
                    'params' => array('exception' => $exception, 'controllerName' => $dispatcher->getControllerName(), 'actionName' => $dispatcher->getActionName())
                ));

                return $event->isStopped();
                break;
            case \Phalcon\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
            case \Phalcon\Dispatcher::EXCEPTION_INVALID_HANDLER:
            case \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
            case \Phalcon\Dispatcher::EXCEPTION_INVALID_PARAMS:

                $dispatcher->forward(array(
                    'controller' => 'error',
                    'action' => 'pageNotFound',
                    'params' => array('exception' => $exception)
                ));

                return $event->isStopped();
                break;
            default:

                break;
        }
    }
}