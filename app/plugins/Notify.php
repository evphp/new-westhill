<?php
//namespace Phalcon\Plugin;

use Phalcon\Mvc\View\Engine\Volt as VoltEngine,
    Phalcon\Mvc\View,
    Phalcon\Mvc\User\Plugin;
    use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;

class Notify extends Plugin
{

    public $lang;
    public $dbname;

    private $mail_conf;
    private $sms_conf;
    private $debugDir;
    private $view;
    private $tx;

    protected $_transport;

    function __construct()
    {
        require_once(realpath(__DIR__) . '/Swift/swift_required.php');

        $conf = $this->getDI()->get('config');
        $lang = $this->getDI()->get('config')->application->defaultLang;
        if ($this->getDI()->has('session')) {
            $lang = $this->getDI()->get('session')->has('lang') ? $this->getDI()->get('session')->get('lang') : $lang;
            // Now we're getting the best language for the user
        }

        $this->tx = new Library\Texts\Adapter\Database([
            'db' => $this->getDI()->get('db'), // Here we're getting the database from DI
            'table' => 'texts', // The table that is storing the translations
            'language' => $lang
        ]);

        $this->view = new View();
        //$this->view->setViewsDir('../app/views/email_views/');
        $this->view->setViewsDir($conf->application->viewsDir.'emails/');

        $this->view->setDI($this->getDI());

        $this->view->registerEngines(array(
            '.volt' => function ($view, $di) {
                $volt = new VoltEngine($view, $di);
                $volt->setOptions(array(
                    'compiledPath' => $di->get('config')->application->cacheDir, // '../cache/',
                    'compiledSeparator' => '_'
                ));
                $compiler = $volt->getCompiler();
                $volt->getCompiler()->addFunction('is_a', 'is_a');
                $volt->getCompiler()->addFunction('substr', 'substr');

                $volt->getCompiler()->addFunction('tx', function ($resolvedArgs, $exprArgs) {
                    return sprintf('$this->tx->query(%s)', $resolvedArgs);
                });

                return $volt;
            }
        ));
        $this->mail_conf = $conf->mail;
        $this->sms_conf = $conf->sms;
        $this->config = $conf;

        // register the translate service if we use sendEmail in the tasks (CLI)
        if ($this->getDI()->has('tx') === false) {
            $di = $this->getDI();
            $di->setShared('tx', function() use ($di) {
                $language = $di->getConfig()->application->defaultLang;
                if ($di->has('session') && $di->getShared('session')->has('lang') && $di->getShared('session')->get('lang')!== '') {
                    $language = $di->getShared('session')->get('lang');
                }
                return new Library\Texts\Adapter\Database([
                    'db' => $di->get('db'), // Here we're getting the database from DI
                    'table' => 'texts', // The table that is storing the translations
                    'language' => $language, // Now we're getting the best language for the user
                ]);
            });
        }
    }



    public function sendSmsNotification($to, $message, $isHigh = 0)
    {

	    $smsConf = $this->getDI()->get('config')->sms;
        if (!$to || !$message){
            return false;
        }

        if (mb_strlen($message) > 402){
            return false;
        }

        $parameters = array_merge(array('to' => $to, 'message' => $message), (array)$smsConf);
        if($isHigh == 1){
            $parameters = array_merge(array('fast'=>1), $parameters);
        }

        $data = '?'.http_build_query($parameters);
        //echo $data;
        $file = fopen('https://api.smsapi.com/sms.do'.$data, 'r');
        $result = fread($file, 1024);

        return strpos($result, 'OK');

    }

    /**
     * Applies a template to be used in the e-mail
     *
     * @param string $name
     * @param array $params
     */
    public function getTemplate($name, $params)
    {
        if (!is_array($params)){
            $params = array();
        }
        //$parameters = array_merge(array('publicUrl' => $this->url->getBaseUri()), $params);
        $parameters = $params;

        if(is_array($name) && count($name) == 2) {
	        list($folder, $template) = $name;
        } else {
        	$folder = 'index';
        	$template = 'index';
        }

        $this->view->setRenderLevel(View::LEVEL_LAYOUT);
        return $this->view->getRender($folder, $template, $parameters);
    }

    /**
     * Sends e-mails based on predefined templates
     *
     * @param array $to
     * @param string $subject
     * @param array $templateArray [folder/file]
     * @param array $params
     * @return bool
     */
    public function sendEmail($to, $subject, $templateArray = [], $from = [], $params = [], $attachArr = [])
    {
        if (is_array($templateArray) && count($templateArray) === 2) {
            list($folder, $template) = $templateArray;
        } else {
            $folder = 'index';
            $template = 'index';
        }

        $mailConfig = $this->getDI()->get('config')->mail;

        if (count($from) == 0) {
            $from = [$mailConfig->fromEmail => $mailConfig->fromName];
        }

        $swiftMessage = Swift_Message::newInstance()
            ->setSubject($subject)
            ->setTo($to)
            ->setFrom($from);

        $logotypePath = $this->config->application->publicDir.'img/logotype.jpg';

        if (file_exists($logotypePath)) {
            $logoCID = $swiftMessage->embed(Swift_Image::fromPath($logotypePath));
            $this->view->setVar('logoCID', $logoCID);
        }
        if (count($attachArr)) {
            foreach ($attachArr AS $filePath) {
                if (file_exists($filePath)) {
                    $swiftMessage->attach(Swift_Image::fromPath($filePath));
                }
            }
        }

        // get Plain email template
        $view = clone $this->view;
        $view->start();
        $view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $view->render($folder, $template, $params);
        $view->finish();
        $plain_template = strip_tags($view->getContent());

        $templateX = $this->view->getRender($folder, $template, $params);

        $swiftMessage->setBody($templateX, 'text/html');
        $swiftMessage->addPart($plain_template, 'text/plain');

        if (!$this->_transport) {
            $this->_transport = Swift_SmtpTransport::newInstance(
                $mailConfig->smtp->server,
                $mailConfig->smtp->port,
                $mailConfig->smtp->security
            )->setUsername($mailConfig->smtp->username)->setPassword($mailConfig->smtp->password);
        }

        $mailer = Swift_Mailer::newInstance($this->_transport);
        return $mailer->send($swiftMessage);
    }

    function sendDebug($params)
    {
        $templateX = $this->getTemplate(array("developer","UserController"), $params);

        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
        $plain_template = $this->view->getRender("developer","UserController",$params);


        //$fileName = $this->debugDir.''.date("d")."-".date("m")."-".date("Y")." G:i:s.txt";
        //$file = fopen($fileName,"w");
        //fwrite($file,$params['message']);
        //fclose($file);

	    $mailConfig = $this->getDI()->get('config')->mail;


        $from = array(
	        $mailConfig->fromEmail => $mailConfig->fromName
        );

        $message = Swift_Message::newInstance()
            ->setSubject("Problem on website")
            ->setTo($mailConfig->developerMail)
            ->setFrom($from)
            ->setBody(strip_tags($plain_template))
            ->addPart($templateX, 'text/html');

        if (!$this->_transport) {
            $this->_transport = Swift_SmtpTransport::newInstance(
	            $mailConfig->smtp->server,
	            $mailConfig->smtp->port,
	            $mailConfig->smtp->security
            )->setUsername($mailConfig->smtp->username)->setPassword($mailConfig->smtp->password);
        }

        $mailer = Swift_Mailer::newInstance($this->_transport);
        return $mailer->send($message);
    }

    function sendNotificationEmail($notificationId)
    {
        $notification = \Notification::findFirstByNotificationId($notificationId);

        if(!$notification){
            throw new Exception("No notification with this id");
        }else {
            if ($notification->isSent == 0 || $notification->isSent == NULL) {

                if($notification->notificationModel == "User"){
                    $notificationUser = User::findFirstByUserId($notification->userId);
                    //@Todo check user settings for notifications */
                    if ($notification->notificationAction == "Approve") {
                        $notificationTempltate = array('notifications', 'UserApproved');
                    } else {
                        $notificationTempltate = array('notifications', 'UserDecline');
                    }

                    try {
                        $this->sendEmail($notificationUser->email, $notification->notificationTitle, array(), array(), array(
                                'userObj' => $notificationUser,
                                'notificationMessage' => $notification->notificationMessage,
                                'notificationComments' => $notification->notificationComment,
                                'notificationReason' => $notification->notificationReason)
                        );
                        $notification->isSent = 1;
                    } catch (\Swift_TransportException $e) {
                        $notification->isSent = 0;
                    }
                    $notification->save();
                }else{
                        $notificationUser = User::findFirstByUserId($notification->userId);
                        $notificationCompany = Company::findFirstByCompanyId($notification->companyId);
                        $notificationTempltate = array('notifications', 'Default');
                        try {
                            $this->sendEmail($notificationUser->email, $notification->notificationTitle, $notificationTempltate, array(), array(
                                    'userObj' => $notificationUser,
                                    'companyObj'=> $notificationCompany,
                                    'notificationMessage' => $notification->notificationMessage,
                                    'notificationComments' => $notification->notificationComment,
                                    'notificationReason' => $notification->notificationReason)
                            );
                            $notification->isSent = 1;
                        } catch (\Swift_TransportException $e) {
                            $notification->isSent = 0;
                        }
                        $notification->save();
                    }


            }
        }
    }

    public function setMailConf($mail_configuration)
    {
        $this->mail_conf = $mail_configuration;
    }

    public function setSmsConf($sms_configuration)
    {
        $this->sms_conf = $sms_configuration;
    }

}
