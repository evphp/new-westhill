<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <!-- Page title -->
    {{ get_title() }}
    <!--/Page title -->

    <meta name="viewport" content="width=device-width, initial-scale=0.999999, user-scalable=no, viewport-fit=cover" />
    <meta name="format-detection" content="telephone=no"/>
{% if metaDescription is defined %}
    <meta name="description" content="{{ metaDescription }}"/>
{% endif %}

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700" />
    <link rel="stylesheet" href="/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/css/style.css{% if stylesVer is defined %}?v={{ stylesVer }}{% endif %}" />
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" href="/css/ie10-viewport-bug-workaround.css" />
    <link rel="stylesheet" href="/css/themify-icons.css" />
    <link rel="stylesheet" href="/css/flag-icon.min.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

    {% if headerCss is defined %}
        {% for cssScript in headerCss %}
            <link rel="stylesheet" href="{{ cssScript }}" type="text/css" />
        {% endfor %}
    {% endif %}

    <meta property="og:url"                content="{{ router.getRewriteUri() }}" />
    <meta property="og:type"               content="article" />
{% if ogMetaTitle is defined %}
    <meta property="og:title"              content="{{ ogMetaTitle }}" />
{% endif %}
{% if ogMetaDescription is defined %}
    <meta property="og:description"        content="{{ ogMetaDescription }}" />
{% endif %}
    <meta property="og:image"              content="{{ config.application.publicUrl }}img/og-picture.png" />

    <!-- iconset -->
    <link rel="icon" type="image/png" href="/img/favicon16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/img/favicon32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/img/favicon48.png" sizes="48x48">
    <link rel="icon" type="image/png" href="/img/favicon96.png" sizes="96x96">

    <!-- ios icon -->
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon152.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon144.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon120.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon114.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon76.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon72.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon60.png">
    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon57.png">
    <!--/ios icon -->

    <!-- Win8 -->
    <meta name="msapplication-TileColor" content="#0c2947">
    <meta name="msapplication-TileImage" content="/img/favicon144.png">
    <!--/Win8 -->

    <meta name="theme-color" content="#0c2947">
    <!--/iconset -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>-->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>-->
    <![endif]-->
</head>

<body data-loader-color="#258ad5">
    <div class="body-loader">
        <div class="cs-loader">
            <div class="cs-loader-inner">
                <label>	●</label>
                <label>	●</label>
                <label>	●</label>
                <label>	●</label>
                <label>	●</label>
                <label>	●</label>
            </div>
        </div>
    </div>


    <nav class="navbar navbar-default navbar-fixed-top" id="headerNavigation">
        <div class="container relative">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed xs-m-t-30 sm-m-t-30" id="navigation-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="headerLogo">
                    <a id="logotype" href="/">
                        <img src="/img/logotype.jpg">
                    </a>
                    <span id="logotype-slogan" class="uppercase">{{ config.siteInfo.slogan }}</span>
                </div>
            </div>

            <div class="collapse navbar-collapse xs-full-width sm-full-width sm-m-t-100 xs-m-t-50 dark-background" id="header-navigation">
                <ul class="nav navbar-nav xs-full-width sm-full-width">
                    <li class="xs-full-width sm-full-width{% if router.getControllerName() == "index" or router.getControllerName() == "" %} active{% endif %}"><a href="/{{ session.lang }}/">{{ tx("page.main.title") }}</a></li>
                    <li class="xs-full-width sm-full-width{% if router.getControllerName() == "about" %} active{% endif %}"><a href="/{{ session.lang }}/about">{{ tx("page.about.title") }}</a></li>
                    <li class="xs-full-width sm-full-width{% if router.getControllerName() == "real_estate_for_sale_in_montenegro" %} active{% endif %}"><a href="/{{ session.lang }}/real_estate_for_sale_in_montenegro">{{ tx("page.sell_property.title") }}</a></li>
                    <li class="xs-full-width sm-full-width{% if router.getControllerName() == "real_estate_for_rent_in_montenegro" %} active{% endif %}"><a href="/{{ session.lang }}/real_estate_for_rent_in_montenegro">{{ tx("page.rent.title") }}</a></li>
                    <li class="xs-full-width sm-full-width{% if router.getControllerName() == "list_a_property_in_montenegro" %} active{% endif %}"><a href="/{{ session.lang }}/list_a_property_in_montenegro">{{ tx("page.collaboration.title") }}</a></li>
                    <li class="xs-full-width sm-full-width{% if router.getControllerName() == "agents" %} active{% endif %}"><a href="/{{ session.lang }}/agents">{{ tx("page.compony_agents.title") }}</a></li>
{#                    <li class="xs-full-width sm-full-width{% if router.getControllerName() == "articles" %} active{% endif %}"><a href="/{{ session.lang }}/articles">{{ tx("page.articles.title") }}</a></li>#}
                    <li class="xs-full-width sm-full-width{% if router.getControllerName() == "contacts" %} active{% endif %}"><a href="/{{ session.lang }}/contacts">{{ tx("page.contacts.title") }}</a></li>
                </ul>
            </div>

            <div class="languageBar text-right">
                {% for lang in config.application.enabledLanguages %}
{#                    {% if lang.iso_code != session.lang %}#}
                        &nbsp;<a href="/{{ lang.iso_code }}/{{ router.getControllerName() }}{% if actionName is defined and actionName is not 'index' %}/{{ actionName }}{% endif %}{% if navLinkParams is defined and navLinkParams %}/{{ navLinkParams }}{% endif %}" title="{{ lang.lang }}"><i class="flag-icon flag-icon-{{ lang.iso_country }}"></i></a>
{#                    {% endif %}#}
                {% endfor %}
            </div>
        </div>
    </nav>

    <div id="topStickyBar">
        <div class="container">
            <div id="errorBox" class="alertBox">
                {% if flashSession.has('error') %}
                    <div class="header-popup error">
                        <div class="header-popup-container">
                            {{  flashSession.output() }}
                        </div>
                        <div class="header-popup-btn">
                            <i class="close-header-popup ti-close pull-right m-t-3"></i>
                        </div>
                    </div>
                {% endif %}
            </div>
        </div>
    </div>

    <!-- Static navbar -->
{#    <nav class="navbar navbar-default">#}
{#        <div class="container-fluid">#}
{#            <div class="navbar-header">#}
{#                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">#}
{#                    <span class="sr-only">Toggle navigation</span>#}
{#                    <span class="icon-bar"></span>#}
{#                    <span class="icon-bar"></span>#}
{#                    <span class="icon-bar"></span>#}
{#                </button>#}
{#                <a class="navbar-brand" href="#">Project name</a>#}
{#            </div>#}
{#            <div id="navbar" class="navbar-collapse collapse">#}
{#                <ul class="nav navbar-nav navbar-right">#}
{#                    <li><a href="#">Static top</a></li>#}
{#                    <li><a href="#">Static top</a></li>#}
{#                    <li><a href="#">Fixed top</a></li>#}
{#                </ul>#}
{#            </div><!--/.nav-collapse -->#}
{#        </div><!--/.container-fluid -->#}
{#    </nav>#}

    {% if headerImages is defined %}
    <div id="headerCarousel" class="xs-hidden sm-hidden _md-hidden">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            {% if headerImages|length > 1 %}
            <ol class="carousel-indicators m-b-0">
                {% for headerImg in headerImages %}
                    <li data-target="#carousel-example-generic" data-slide-to="{{ loop.index - 1 }}"{% if loop.index == 1 %} class="active"{% endif %}></li>
                {% endfor %}
            </ol>
            {% endif %}

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                {% for headerImg in headerImages %}
                    <div class="item{% if loop.index == 1 %} active{% endif %}">
                        <a href="{{ headerImg.link }}"><img src="{{ filePrefix }}{{ headerImg.image }}" alt=""></a>
                        <!--<div class="carousel-caption"><a href="" style="color: white">Porte montenegro</a></div>-->
                    </div>
                {% endfor %}
            </div>
            <!-- Controls -->
            <!--        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">-->
            <!--            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>-->
            <!--            <span class="sr-only">Previous</span>-->
            <!--        </a>-->
            <!--        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">-->
            <!--            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>-->
            <!--            <span class="sr-only">Next</span>-->
            <!--        </a>-->
        </div>
    </div>
    {% endif %}

    {{ content() }}

    {% if newObjects is defined %}
        <div class="relative container-fluid xs-p-t-5 sm-p-t-5 p-t-30 xs-sm-p-b-35 p-b-50 xs-sm-p-l-0 xs-sm-p-r-0">
            <div class="container relative">
                <h2>{{ tx('newobjects.title') }}</h2>
                {{ partial("parts/new-objects",["objects":newObjects]) }}
            </div>
        </div>
    {% endif %}

    {#<footer class="sticky-footer">#}
        {#<div class="container">#}
            {#<div class="footer-container__content">#}
                {#&#9400; 2006-2019. West Hill. All rights reserved#}
            {#</div>#}
        {#</div>#}
    {#</footer>#}

    {#<div class="container-fluid dark-background">#}
        {#<div class="container m-t-15 m-b-15">#}
            {#<div class="footer-container__content">#}
                {#&#9400; 2006-2019. West Hill. All rights reserved#}
            {#</div>#}
        {#</div>#}
    {#</div>#}

    <div class="full-width dark-background font-weight-light">
        <div class="container p-t-50 p-b-50">
            <!-- Footer contents -->
            <div class="row text-center">
                <div class="col-sm-12 social-icons-box">
                    {#<a href="#" target="_blank"><i class="fab fa-instagram"></i></a>>#}{{ config.siteInfo.name }} <a href="{{ config.siteInfo.facebook }}" target="_blank"><i class="fab fa-facebook"></i></a>
                    {#<a href="#" target="_blank"><i class="fab fa-instagram"></i></a>#}
                    {#<a href="#" target="_blank"><i class="fab fa-twitter"></i></a>#}
                    {#<a href="#" target="_blank"><i class="fab fa-telegram-plane"></i></a>#}
                    {#<a href="#" target="_blank"><i class="fab fa-medium-m"></i></a>#}
                    {#<a href="#" target="_blank"><i class="fab fa-linkedin"></i></a>#}
                </div>
                <div class="col-sm-12 m-t-10 text-center">
                    Address: {{ config.siteInfo.address }}
                </div>
                <div class="col-sm-12 m-t-10 text-center">
                    Email: <a href="mailto:{{ config.siteInfo.email }}">{{ config.siteInfo.email }}</a>
                </div>
                <div class="col-sm-12 m-t-10 text-center">
                    Phone: {{ config.siteInfo.phone }}
                </div>
                {% if config.siteInfo.phoneExtra %}
                <div class="col-sm-12 m-t-10 text-center">
                    Phone: {{ config.siteInfo.phoneExtra }}
                </div>
                {% endif %}
                <div class="col-sm-12 m-t-10 text-center">
                    Ⓒ 2006-2019. All rights reserved
                </div>
            </div>
        </div>
    </div>

    <script src="/js/jquery-3.4.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    <script src="/js/jquery.magnific-popup.min.js"></script>
    <script src="/js/select2.full.min.js"></script>
    <script src="/js/components/jquery.nicescroll.min.js"></script>
    <script src="/js/components/jquery.nicescroll.iframehelper.js"></script>
    <script src="/js/fastclick.js"></script>
    <script src="/js/main.js{% if mainJSVer is defined %}?v={{ mainJSVer }}{% endif %}"></script>

    {% if townsList is defined %}
        <script>
            let townsList = {{ townsList }};
        </script>
    {% endif %}

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/js/ie10-viewport-bug-workaround.js"></script>

    {% if config.application.filePrefix is '' %}
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-98329809-1', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- BEGIN JIVOSITE CODE {literal} -->
    <script type='text/javascript'>
        (function(){ var widget_id = '{{ tx('jivochat.widget_id') }}';var d=document;var w=window;function l(){
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
    <!-- {/literal} END JIVOSITE CODE -->
    {% endif %}

    {% if footerJs is defined %}
        {% for jsScript in footerJs %}
            <script src="{{ jsScript }}"></script>
        {% endfor %}
    {% endif %}
</body>
</html>