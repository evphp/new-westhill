<div id="smallPageWrapper">
    <div class="full-height relative">
        <div class="container p-t-100 p-b-100 xs-sm-p-b-50">
            <h1 class="m-t-100 xs-sm-m-t-50">{{ tx('page.rent.title') }}</h1>
            <div class="m-t-20">
                {{ tx('page.rent.content') }}
            </div>
        </div>
    </div>
</div>