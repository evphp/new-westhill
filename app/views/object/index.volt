{{ partial("parts/filter",["class":"filter-page__object"]) }}

<div class="__p-t-100 object-header">
    <div class="object-header__bg xs-hidden" style="background-image:url({{ config.application.filePrefix ~ object.Image.mid_image }}"></div>
    <div class="object-header__bg-dark xs-hidden"></div>
    <div class="container object-container">
        <div class="row">
            <div class="col-xs-12 xs-padding-0">
                {#<span style="position: absolute; right: 1%; top: 1%;z-index: 200; background-color:  #0c2947;background-color: hsla(50, 33%, 25%, .75);">#}
                    {#<i class="ti-zoom-in" style="font-size: 50px;color:white;cursor: pointer"></i>#}
                {#</span>#}
                <div class="object-header__preview text-center">
                    {% for image in object.Images %}
                        {#<div class="object-header__preview-img" style="background-image:url({{ config.application.filePrefix ~ image.big_image }}"></div>#}
                        <div class="text-center"><img src="{{ config.application.filePrefix ~ image.big_image }}"></div>
                    {% endfor %}
                </div>
            </div>
            {% if object.Images|length > 1 %}
            <div class="col-xs-12">
                <div class="object-header__nav text-center m-t-10 xs-hidden">
                    {% for image in object.Images %}
                        <div><img src="{{ config.application.filePrefix ~ image.small_image }}" alt=""></div>
                    {% endfor %}
                </div>
            </div>
            {% endif %}
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="container">
    <div class="row xs-m-t-30 sm-m-t-30 m-t-80 xs-sm-p-b-50 p-b-100">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
            <h1 class="row md-row-eq-height lg-row-eq-height p-l-15 p-r-15">
                <span class="pull-left md-full-width lg-full-width">{{ tx('re_object.name.' ~ object.id, object.name) }}</span>
                {% if object.getPrice() > 0 %}
                <span class="md-pull-right lg-pull-right sm-pull-left xs-pull-left sm-full-width xs-full-width  sm-m-t-10 xs-m-t-10">
                    <span class="sm-m-t-3 xs-m-t-3 lg-pull-right md-pull-right sm-pull-left xs-pull-left text-nowrap">{{ object.getPrice() }} €</span>
                </span>
                {% endif %}
            </h1>
            <div class="clearfix"></div>
            {% if object.getPrice() == 0 %}
                <h2 class="m-t-0">{{ tx('list.negotiatedprice') }}</h2>
            {% endif %}
            {% if object.firstline is 'Y' %}
            <div class="row m-t-10">
                <div class="col-sm-12">
                    <span class="label label-orange f-s-20">{{ tx('object.firstline') }}</span>
                </div>
            </div>
            {% endif %}
            {% if object.re_rental_frequency_id > 1 %}
            <div class="row m-t-10">
                <div class="col-sm-12">
                    <h4>{{ tx('re_deal_type.name.'~object.re_rental_frequency_id) }}</h4>
                </div>
            </div>
            {% endif %}
            <div class="row sm-row-eq-height md-row-eq-height lg-row-eq-height m-t-15">
                <div class="col-sm-8">
                    <span class="p-t-0 p-b-0 m-t-0 m-b-0">
                        <img src="/img/svg/geotag.svg" height="18" class="pull-left m-r-5">
                        {{ tx('re_town.name.' ~ object.re_town_id) }}{% if object.ReTown.re_region_id %}, ({{ tx('region.' ~ object.ReTown.re_region_id) }}){% endif %}<br>
                        {#<a href="#" class="location-details">Подробнее о регионе</a>#}
                    </span>
                </div>
                <div class="col-xs-12 col-sm-4 xs-text-left xs-m-t-10 sm-text-right md-text-right lg-text-right">
                    <span class="color-gray">{{ tx('object.id') }}: {{ object.id }}</span>
                </div>
            </div>
            {% set attr_types_id_array = {22, 6, 14, 32, 38, 68, 24, 75, 15, 35, 80, 82, 20, 4, 16, 30, 37, 79, 19, 1, 74, 31} %}
            <ul class="attributes m-t-30">
                {% for attr in object.Attributes %}
                    {% if attr.value and attr.AttributeType and in_array(attr.re_attribute_types_id, attr_types_id_array) %}
                        {% if in_array(attr.re_attribute_types_id, {24, 75, 35}) %}
                            <li>
                                <img src="/img/svg/bedrooms_number.svg" width="40">
                                <span>{{ attr.value }}</span>
                                <div class="attributes_subtext">{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                            </li>
                        {% elseif in_array(attr.re_attribute_types_id, {6, 22, 32, 38}) %}
                            <li>
                                <img src="/img/svg/area.svg" width="40">
                                <span>{{ attr.value }} {{ attr.AttributeType.unit }}</span>
                                <div class="attributes_subtext">{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                            </li>
                        {% elseif in_array(attr.re_attribute_types_id, {4, 20, 16, 30, 82}) %}
                            <li>
                                <img src="/img/svg/distance_to_sea.svg" width="40">
                                <span>{{ attr.value }} {{ attr.AttributeType.unit }}</span>
                                <div class="attributes_subtext">{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                            </li>
                        {% elseif in_array(attr.re_attribute_types_id, {15, 19}) %}
                            {% if in_array(attr.value,['Y','N']) and attr.value is 'Y' %}
                            <li>
                                <img src="/img/svg/sea_view.svg" width="40">
                                {#<span>#}
                                    {#{{ tx("yes") }}#}
                                {#</span>#}
                                <div class="attributes_subtext">{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                            </li>
                            {% endif %}
                        {% elseif in_array(attr.re_attribute_types_id, {1, 14}) %}
                            <li>
                                <img src="/img/svg/plot_size.svg" width="40">
                                <span>{{ attr.value }} {{ attr.AttributeType.unit }}</span>
                                <div class="attributes_subtext">{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                            </li>
                        {% elseif in_array(attr.re_attribute_types_id, {31, 37})%}
                            <li>
                                <img src="/img/svg/floor_storeys.svg" width="40">
                                <span>{{ attr.value }} {{ attr.AttributeType.unit }}</span>
                                <div class="attributes_subtext">{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                            </li>
                        {% elseif attr.re_attribute_types_id is 68 %}
                            {% if in_array(attr.value,['Y','N']) and attr.value is 'Y' %}
                            <li>
                                <img src="/img/svg/venture_capital.svg" width="40">
                                {#<span>#}
                                     {#{{ tx("yes") }}#}
                                {#</span>#}
                                <div class="attributes_subtext">{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                            </li>
                            {% endif %}
                        {% elseif attr.re_attribute_types_id is 80 %}
                            <li>
                                <img src="/img/svg/construction_year.svg" width="40">
                                <span>{{ attr.value }} {{ attr.AttributeType.unit }}</span>
                                <div class="attributes_subtext">{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                            </li>
                        {% elseif attr.re_attribute_types_id is 79 %}
                            <li>
                                <img src="/img/svg/roi.svg" width="40">
                                <span>{{ attr.value }} {{ attr.AttributeType.unit }}</span>
                                <div class="attributes_subtext">{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                            </li>
                        {% elseif attr.re_attribute_types_id is 74 %}
                            {% if in_array(attr.value,['Y','N']) and attr.value is 'Y' %}
                            <li>
                                <img src="/img/svg/agricultural_plot.svg" width="40">
                                {#<span>#}
                                    {#{{ tx("yes") }}#}
                                {#</span>#}
                                <div class="attributes_subtext">{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                            </li>
                            {% endif %}
                        {% endif %}
                    {% endif %}
                {% endfor %}
            </ul>
            <div class="row">
                <div class="col-xs-12 col-md-6 col-lg-6">
                    <ul class="item-attributes">
                        {% for attr in object.Attributes %}
                            {% if attr.value and attr.AttributeType and not in_array(attr.re_attribute_types_id, attr_types_id_array) %}
                                <li>
                                    <span class="attribute-name">{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</span>
                                    <span class="attribute-value">
                                    {% if attr.AttributeType.valuetype is 'I' %}
                                        {{ attr.intvalue }} {{ attr.AttributeType.unit }}
                                    {% else %}
                                        {% if attr.AttributeType.controltype is 3 %}
                                            {% if attr.value is 'Y' %}
                                                {{ tx("yes") }}
                                            {% else %}
                                                {{ tx("no") }}
                                            {% endif %}
                                        {% else %}
                                            {{ attr.value }} {{ attr.AttributeType.unit }}
                                        {% endif %}
                                    {% endif %}
                                    </span>
                                </li>
                            {% endif %}
                        {% endfor %}
                    </ul>
                </div>
            </div>
            <h2>{{ tx('object.description') }}</h2>
            {{ tx('re_object.description.' ~ object.id) }}
            <div class="clearfix"></div>
            {% set additionalInfo = object.additionalHtmlFromCSV() %}
            {% if additionalInfo is not '' %}
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="m-t-50">{{ tx('object.features', 'Features') }}</h2>
                </div>
                <div class="col-xs-12 p-l-35">
                    {{ object.additionalHtmlFromCSV() }}
                </div>
            </div>
            {% endif %}
            <div class="full-width text-center m-t-50">
                <a href="{{ config.application.backendSite }}/{{ session.lang }}/print/{{ object.id }}/{{ 'object_' ~ object.id ~ '.pdf' }}" class="btn btn-secondary btn-lg"><i class="ti-download"></i> {{ tx('print') }}</a>
            </div>
            <div class="row item-links m-r-0 m-l-0">
                {#<div class="col-xs-12"><a href="#">{{ tx('seetown') }}</a></div>#}
                <div class="col-xs-12">
                    <a href="/?fs=1&type={{ object.re_types_id }}&town={{ object.re_town_id }}">{{ rtx('object.see-all.same-type-and-town', {'$type' : tx('re_types.name.' ~ object.re_types_id), '$town' : tx('re_town.name.' ~ object.re_town_id)}) }}</a>
                </div>
                <div class="col-xs-12">
                    <a href="/?fs=1&type={{ object.re_types_id }}">{{ rtx('object.see-all.same-type', {'$type' : tx('re_types.name.' ~ object.re_types_id)}) }}</a>
                </div>
                <div class="col-xs-12">
                    <a href="/?fs=1">{{ tx('object.see-all') }}</a>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 xs-m-t-30 sm-m-t-30 md-m-t-30">
            {{ partial("parts/write-to-us") }}
        </div>
    </div>
</div>
<div class="clearfix"></div>