{#<div class="container-fluid p-t-100 p-b-60">#}
    {#<div class="container p-t-40">#}
        {#<h1>{{ tx('page.compony_agents.title') }}</h1>#}
        {#<div class="m-t-20">#}
            {#{{ tx('page.compony_agents.content') }}#}
        {#</div>#}
    {#</div>#}
{#</div>#}
<div id="smallPageWrapper">
    <div class="full-height relative">
        <div class="container p-t-100 p-b-100 xs-sm-p-b-50">
            <h1 class="m-t-100 xs-sm-m-t-50">{{ tx('page.compony_agents.title') }}</h1>
            <div class="m-t-20">
                {{ tx('page.compony_agents.content') }}
            </div>
        </div>
    </div>
</div>