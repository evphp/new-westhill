{{ partial("parts/filter",["class":"filter-page__main"]) }}

<div class="relative container-fluid xs-p-t-30 sm-p-t-30 p-t-80 xs-sm-p-l-0 xs-sm-p-r-0">
    <div class="container relative">
        {% if isFiltered %}
            <h2>{{ filterTitle }}</h2>
            {{ partial("parts/pagination-objects",['page':page,'getParams':strQuery]) }}
        {% else %}
            {{ tx('default.toptext') }}
            {{ partial("parts/new-objects",["objects":filterObjects]) }}
        {% endif %}
    </div>
</div>

<div class="container-fluid xs-p-b-25 sm-p-b-25 p-b-50 xs-p-t-5 sm-p-t-5 p-t-30">
    <div class="container">
        {{ tx('default.bottomtext') }}
    </div>
</div>