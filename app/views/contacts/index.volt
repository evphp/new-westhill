<div class="container-fluid p-t-100">

    <div class="container p-t-80 xs-sm-p-t-30 p-b-100 xs-sm-p-b-50">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                <h1>{{ tx('page.contacts.title') }}</h1>
                <div class="m-t-20">
                    {{ tx('page.contacts.content') }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 xs-m-t-30 sm-m-t-30 md-m-t-30">
                {{ partial("parts/write-to-us") }}
            </div>
        </div>
    </div>

</div>