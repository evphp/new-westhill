<div id="smallPageWrapper">
    <div class="full-width full-height relative ">

        <div class="container full-height lg-center-vertical md-center-vertical sm-center-vertical xs-center-vertical">
            <div class="errorWrapper text-center">
                <h1 class="error404 center">500</h1>
                Ops! Page is not responding

                {% if session.isOffice == 1 %}
                    <div class="well well-lg">
                        {{ errorMessageForAdmin }}
                    </div>
                {% endif %}
            </div>
        </div>
    </div>
</div>