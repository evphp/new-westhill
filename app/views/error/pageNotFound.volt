<div id="smallPageWrapper">
    <div class="full-width full-height relative ">
        {#<div class="diagonal-top-left">#}
        {#<div class="dark-triangle"></div>#}
        {#<div class="blue-triangle"></div>#}
        {#<div class="red-triangle"></div>#}
        {#</div>#}
        {#<div class="diagonal-bottom-right">#}
        {#<div class="dark-triangle"></div>#}
        {#<div class="blue-triangle"></div>#}
        {#<div class="red-triangle"></div>#}
        {#</div>#}


        <div class="container full-height lg-center-vertical md-center-vertical sm-center-vertical xs-center-vertical">
            <div class="errorWrapper text-center">
                <h1 class="error404 center">404</h1>
                Ops! Page you are looking for is not here.
            </div>
        </div>
    </div>
</div>