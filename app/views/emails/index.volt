<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title></title>

    <style type="text/css">

        /* Outlines the grid, remove when sending */
        /*table td { border:1px solid cyan; }*/

        /* CLIENT-SPECIFIC STYLES */
        body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
        table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
        img { -ms-interpolation-mode: bicubic; }

        /* RESET STYLES */
        img { border: 0; outline: none; text-decoration: none; }
        table { border-collapse: collapse !important; }
        body { margin: 0 !important; padding: 0 !important; width: 100% !important; }

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        @media all and (max-width:639px){
            .wrapper{ width:320px!important; padding: 0 !important; }
            .container{ width:300px!important;  padding: 0 !important; }
            .mobile{ width:300px!important; display:block!important; padding: 0 !important; }
            .img{ width:100% !important; height:auto !important; }
            *[class="mobileOff"] { width: 0px !important; display: none !important; }
            *[class*="mobileOn"] { display: block !important; max-height:none !important; }
        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] { margin: 0 !important; }

    </style>
</head>
<body style="margin:0; padding:0; background-color:#F2F2F2;">
<center>

    <div style="background-color:#F2F2F2; max-width: 640px; margin: auto;">
        <!--[if mso]>
        <table role="presentation" width="640" cellspacing="0" cellpadding="0" border="0" align="center">
            <tr>
                <td>
        <![endif]-->

        <table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px; width:100%;" bgcolor="">
            <tr>
                <td align="center" valign="top" style="padding:10px;">

                    <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:600px; width:100%;">
                        <tr>
                            <td height="40" style="font-size:40px; line-height:40px;" class="mobileOn"></td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>

        <table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px; width:100%;" bgcolor="#FFFFFF">
            <tr>
                <td align="center" valign="top" style="padding:10px;">

                    <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:600px; width:100%;">
                        <tr>
                            <td align="center" valign="top" style="padding:10px;">
                                <img src="{{ logoCID }}" width="120" height="" style="margin:0; padding:0; border:none; display:block;" border="0" alt="{{ config.siteInfo.email }}" />
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>

        <table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px; width:100%;" bgcolor="#FFFFFF">
            <tr>
                <td align="center" valign="top" style="padding:10px;">

                    <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:600px; width:100%;">
                        <tr>
                            <td align="left" valign="top" style="padding:10px; font-family:'Trebuchet MS',Tahoma,sans-serif;font-size:16px;line-height:21px;color:#555555;">
                                {{ content() }}
                            </td>
                        </tr>
                        <tr>
                            <td height="40" style="font-size:40px; line-height:40px;" class="mobileOn"></td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>

        <table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px; width:100%;" bgcolor="#233a47">
            <tr>
                <td align="center" valign="top" style="padding:10px;">

                    <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:600px; width:100%; color: #FFFFFF">
                        <tr>
                            <td align="center" valign="top" style="padding:10px; font-family:'Trebuchet MS',Tahoma,sans-serif;font-size:14px;line-height:14px;color:#FFFFFF;">
                                {{ tx("For questions, please contact:") }}<br>
                                <a href="mailto:{{ config.siteInfo.email }}" style="color: #89D3FF">{{ config.siteInfo.email }}</a>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>

        <table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px; width:100%;" bgcolor="">
            <tr>
                <td align="center" valign="top" style="padding:10px;">

                    <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:600px; width:100%;">
                        <tr>
                            <td height="40" style="font-size:40px; line-height:40px;" class="mobileOn"></td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>

        <!--[if mso]>
        </td>
        </tr>
        </table>
        <![endif]-->
    </div>

</center>
</body>
</html>