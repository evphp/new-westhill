<form method="post" name="contactForm" id="contactForm" autocomplete="off">
    <input id="cfCSRF" type="hidden" name="<?= $this->security->getTokenKey() ?>" value="<?= $this->security->getToken() ?>"/>
    <input type="hidden" name="googleReCaptchaSiteKey" value="{{ config.googleRecaptcha.v2.public_key }}" data-version="2"/>
    {% if object is defined and object.id %}
        <input type="hidden" name="object" value="{{ object.id }}"/>
    {% endif %}
    <div class="dark-background full-width relative p-t-30 p-b-30 ask-a-question">
        <div class="col-sm-12 p-l-25 p-r-25">
            <h3 class="color-white m-t-0">Ask a question</h3>
            <div class="form-group m-t-20">
                <label for="cfName">Name</label>
                {{ contactForm.render('cfName') }}
            </div>
            <div class="form-group m-t-20">
                <label for="cfEmail">E-mail</label>
                {{ contactForm.render('cfEmail') }}
            </div>
            <div class="form-group m-t-20">
                <label for="cfMessage">Question</label>
                {{ contactForm.render('cfMessage') }}
            </div>
            <div class="form-group m-t-10">
                <div id="g-recaptcha" style="display:none"></div>
            </div>
            <button class="btn btn-secondary btn-lg full-width inverse m-t-20">{{ tx('Send') }}</button>
        </div>
    </div>
</form>