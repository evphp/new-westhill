<div class="relative full-width dark-background {% if headerImages is defined %}p-t-20{% else %}p-t-100{% endif %} p-b-5 xs-p-t-100 sm-p-t-100 {{ class }}">
    <div class="container {% if headerImages is defined %}xs-p-t-30 sm-p-t-30 md-p-t-30{% else %}p-t-30{% endif %} relative" id="formBox">
        <form method="get" action="/" name="filterForm" id="filterForm" class="m-b-0 filterForm">
            <div class="row md-row-eq-height lg-row-eq-height">
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="type">{{ tx('menu.serach.dealType', 'Deal type') }}<small></small></label>
                        {{ filterForm.render('deal') }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="type">{{ tx('menu.serach.type') }}<small></small></label>
                        {{ filterForm.render('type') }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="region">{{ tx('menu.serach.region') }}<small></small></label>
                        {{ filterForm.render('region') }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="town">{{ tx('menu.serach.town') }}</label>
                        {{ filterForm.render('town') }}
                    </div>
                </div>
            </div>
            <div class="row sm-m-t-15 xs-m-t-15 md-row-eq-height lg-row-eq-height">
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="minprice">{{ tx('price') }}</label>
                        <div class="row">
                            <div class="col-xs-6">
                                {{ filterForm.render('minprice') }}
                            </div>
                            <div class="col-xs-6">
                                {{ filterForm.render('maxprice') }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="object">{{ tx('object.id') }}</label>
                        {{ filterForm.render('object') }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 vertical-middle">
                    <div class="checkbox check-success lg-m-t-30 md-m-t-30 sm-m-t-30">
                        {{ filterForm.render('firstline') }}
                        <label for="firstline" class="vertical-middle p-t-12" style="white-space: normal;">{{ tx('menu.serach.firstline') }}</label>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="form-group">
                        <input type="submit" class="btn btn-secondary btn-lg full-width inverse" name="filterSubmit" value="{{ tx('menu.jump.submit') }}">
                    </div>
                </div>
            </div>
            {#<div class="row m-t-25 p-t-25 border-top-light md-row-eq-height lg-row-eq-height">#}
            {#<div class="col-xs-12 col-sm-push-6 col-md-push-8 col-lg-push-8 col-sm-6 col-md-4 col-lg-4">#}
            {#<input type="submit" class="btn btn-secondary btn-lg full-width inverse" name="filterSubmit" value="{{ tx('menu.jump.submit') }}">#}
            {#</div>#}
            {#</div>#}
        </form>
    </div>
    <div class="container">
        <div class="row text-center">
            <div class="col-xs-12">
                {% if headerImages is defined %}
                    {% set isHeaderImages = true %}
                {% else %}
                    {% set isHeaderImages = false %}
                {% endif %}
                <a href="#" onclick="hideFilter({{ isHeaderImages }})" id="hideFilterBtn"><i class="ti-angle-double-up"></i></a>
                <a href="#" onclick="showFilter({{ isHeaderImages }})" id="showFilterBtn" class="filter-toggle-box{% if not isHeaderImages %} p-t-5{% endif %}"> {{ tx('filter.show.btn') }} <i class="ti-search"></i></a>
            </div>
        </div>
    </div>
</div>