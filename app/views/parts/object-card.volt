<div class="flex-item xs-pull-left estate-item">
    <div class="full-width object-card relative">
        <div class="object-image-list">
            <div class="object-image">
                <a href="/{{ session.lang }}/object/{{ object.id }}"><img src="{{ config.application.filePrefix ~ object.Image.mid_image }}" class="full-width"></a>
            </div>
        </div>
        <div class="col-xs-12 _bg-white p-l-25 p-r-25 m-b-10">
            <h4 class="m-t-20"><a href="/{{ session.lang }}/object/{{ object.id }}">{{ tx('re_object.name.' ~ object.id) }} #{{ object.id }}</a></h4>
            <div class="row row-eq-height">
                <div class="col-sm-1">
                    <img src="/img/svg/geotag.svg" width="16" class="pull-left">
                </div>
                <div class="col-sm-11">
                    <span class="color-orange f-s-14 font-weight-semibold m-t-0-i m-b-0">{{ tx('re_town.name.' ~ object.re_town_id) }}, {{ tx('region.' ~ object.ReTown.re_region_id) }}</span>
                </div>
            </div>
            <div class="row _m-b-25 row-eq-height">
                <div class="col-xs-12">
                    <span class="font-weight-semibold m-t-10 f-s-20 color-blue _text-nowrap">{% if object.getPrice() > 0 %}{{ object.getPrice() }} €{% else %}{{ tx('list.negotiatedprice') }}{% endif %}</span>
                    {% if object.re_rental_frequency_id > 1 %}
                    <div>{{ tx('re_deal_type.name.'~object.re_rental_frequency_id) }}</div>
                    {% endif %}
                </div>
                {#<div class="col-xs-6">#}
                {#<a href="/object/{{ object.id }}" class="btn btn-primary full-width">Посмотреть</a>#}
                {#</div>#}
            </div>
            <div class="row m-b-15 row-eq-height">
                <div class="col-xs-12">
                    {% set attr_types_id_array = {22, 6, 14, 32, 38, 68, 24, 75, 15, 35, 80, 82, 20, 4, 16, 30, 37, 79, 19, 1, 74, 31} %}
                    <ul class="attributes attr-small m-t-10 m-b-10">
                        {% for attr in object.Attributes %}
                            {% if attr.value and attr.AttributeType and in_array(attr.re_attribute_types_id, attr_types_id_array) %}
                                {% if in_array(attr.re_attribute_types_id, {24, 75, 35}) %}
                                    <li>
                                        <img src="/img/svg/bedrooms_number.svg" width="40">
                                        <div class="p-t-2">{{ attr.value }}</div>
                                        <div>{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                                    </li>
                                {% elseif in_array(attr.re_attribute_types_id, {6, 22, 32, 38}) %}
                                    <li>
                                        <img src="/img/svg/area.svg" width="40">
                                        <div class="p-t-2">{{ attr.value }} {{ attr.AttributeType.unit }}</div>
                                        <div>{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                                    </li>
                                {% elseif in_array(attr.re_attribute_types_id, {4, 20, 16, 30, 82}) %}
                                    <li>
                                        <img src="/img/svg/distance_to_sea.svg" width="40">
                                        <div class="p-t-2">{{ attr.value }} {{ attr.AttributeType.unit }}</div>
                                        <div>{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                                    </li>
                                {% elseif in_array(attr.re_attribute_types_id, {15, 19}) %}
                                    {% if in_array(attr.value,['Y','N']) and attr.value is 'Y' %}
                                        <li>
                                            <img src="/img/svg/sea_view.svg" width="40">
                                            {#<div class="p-t-2">#}
                                            {#{{ tx("yes") }}#}
                                            {#</div>#}
                                            <div class="p-t-2">{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                                        </li>
                                    {% endif %}
                                {% elseif in_array(attr.re_attribute_types_id, {1, 14}) %}
                                    <li>
                                        <img src="/img/svg/plot_size.svg" width="40">
                                        <div class="p-t-2">{{ attr.value }} {{ attr.AttributeType.unit }}</div>
                                        <div>{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                                    </li>
                                {% elseif in_array(attr.re_attribute_types_id, {31, 37})%}
                                    <li>
                                        <img src="/img/svg/floor_storeys.svg" width="40">
                                        <div class="p-t-2">{{ attr.value }} {{ attr.AttributeType.unit }}</div>
                                        <div>{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                                    </li>
                                {% elseif attr.re_attribute_types_id is 68 %}
                                    {% if in_array(attr.value,['Y','N']) and attr.value is 'Y' %}
                                        <li>
                                            <img src="/img/svg/venture_capital.svg" width="40">
                                            {#<div class="p-t-2">#}
                                            {#{{ tx("yes") }}#}
                                            {#</div>#}
                                            <div class="p-t-2">{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                                        </li>
                                    {% endif %}
                                {% elseif attr.re_attribute_types_id is 80 %}
                                    <li>
                                        <img src="/img/svg/construction_year.svg" width="40">
                                        <div class="p-t-2">{{ attr.value }} {{ attr.AttributeType.unit }}</div>
                                        <div>{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                                    </li>
                                {% elseif attr.re_attribute_types_id is 79 %}
                                    <li>
                                        <img src="/img/svg/roi.svg" width="40">
                                        <div class="p-t-2">{{ attr.value }} {{ attr.AttributeType.unit }}</div>
                                        <div>{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                                    </li>
                                {% elseif attr.re_attribute_types_id is 74 %}
                                    {% if in_array(attr.value,['Y','N']) and attr.value is 'Y' %}
                                        <li>
                                            <img src="/img/svg/agricultural_plot.svg" width="40">
                                            {#<div class="p-t-2">#}
                                            {#{{ tx("yes") }}#}
                                            {#</div>#}
                                            <div class="p-t-2">{{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}</div>
                                        </li>
                                    {% endif %}
                                {% endif %}
                            {% endif %}
                        {% endfor %}
                    </ul>
                </div>
            </div>
            {#<div class="row m-b-25">#}
            {#<div class="col-xs-12">#}
            {#<a href="/object/{{ object.id }}" class="btn btn-primary full-width">{{ tx('see') }}</a>#}
            {#</div>#}
            {#</div>#}
        </div>
        {#<div class="object-bottom-btn">#}
        {#<a href="/object/{{ object.id }}" class="btn btn-primary full-width">{{ tx('see') }}</a>#}
        {#</div>#}
        {% if object.firstline is 'Y' %}
            <a href="/{{ session.lang }}/object/{{ object.id }}">
                <div class="coming-soon">
                    <div class="coming-soon-text rotate--45">{{ tx('object.firstline') }}</div>
                </div>
            </a>
        {% endif %}
    </div>
</div>