<div class="flex-container full-width m-l-0 m-r-0 m-t-25 xs-m-b-5 sm-m-b-5 m-b-35">
{% if objects is defined and objects|length > 0 %}
    {% for object in objects %}
        {{ partial("parts/object-card", ["object":object]) }}
    {% endfor %}
{% else %}
    <div class="object-card p-l-10 p-t-20 p-r-10 p-b-20 m-b-15">
        {{ tx('error.objectnotfound') }}
    </div>
{% endif %}
</div>
