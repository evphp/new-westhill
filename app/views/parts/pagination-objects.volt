{%- macro get_paginator_arr(currPage, maxPages) %}
    {%- set array = [] -%}
    {%- if maxPages > 9 -%}
        {%- if currPage < 4 -%}
            {%- for index in 1..5 -%}
                {%- set array[index] = index -%}
            {%- endfor -%}
            {%- set array[0] = '0' -%}
            {%- set array[maxPages] = maxPages -%}
        {%- elseif currPage > maxPages - 3 -%}
            {%- set array[1] = 1 -%}
            {%- set array[0] = '0' -%}
            {%- for index in maxPages - 4..maxPages -%}
                {%- set array[index] = index -%}
            {%- endfor -%}
        {%- else -%}
            {%- set array[1] = 1 -%}
            {%- set array['0'] = '0' -%}
            {%- for index in currPage - 1..currPage + 1 -%}
                {%- set array[index] = index -%}
            {%- endfor -%}
            {%- set array['00'] = '0' -%}
            {%- set array[maxPages] = maxPages -%}
        {%- endif -%}
    {%- else -%}
        {%- for index in 1..maxPages -%}
            {%- set array[index-1] = index -%}
        {%- endfor -%}
    {%- endif -%}
    {%- return array -%}
{%- endmacro %}

{% if 0 %}
    <div class="full-width m-l-0 m-r-0 {% if page is defined and page.total_pages > 1 %}m-t-10 xs-p-b-40 sm-p-b-40 p-b-90{% else %}m-t-25 xs-p-b-20 sm-p-b-20 p-b-70{% endif %}">
        {% if page is defined and page.items|length > 0 %}
            {% if page.total_pages > 1 %}
                {% set paginatorArr = get_paginator_arr(page.current, page.total_pages) %}
                <div class="text-center">
                    <ul class="pagination">
                        <li class="xs-hidden"><a href="/{{ getParams }}&page={{ page.before }}"><span class="ti-angle-double-left"></span></a></li>
                        {% for p in paginatorArr %}
                            {% if p == 0 %}
                                <li class="pagination-dots"><span>...</span></li>
                            {% else %}
                                <li {% if p == page.current %} class="active"{% endif %}><a href="/{{ getParams }}&page={{ p }}">{{ p }} {% if p == page.current %}<span class="sr-only">(current)</span>{% endif %}</a></li>
                            {% endif %}
                        {% endfor %}
                        <li class="xs-hidden"><a href="/{{ getParams }}&page={{ page.next }}"><span class="ti-angle-double-right"></span></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            {% endif %}
            {% for object in page.items if object.re_town_id %}
                <div class="row m-b-30 object-card m-l-0 m-r-0">
                    <div class="col-xs-12 col-md-6 col-lg-5 object-image-list-filtered p-l-0 xs-p-r-0 sm-p-r-0">
                        <div class="object-image">
                            <a href="/object/{{ object.id }}" target="_blank"><img src="{{ config.application.filePrefix ~ object.Image.big_image }}" class="full-width"></a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-7">
                        <h4 class="m-t-20"><a href="/object/{{ object.id }}" target="_blank">{{ tx('re_object.name.' ~ object.id) }}</a></h4>
                        <div class="row row-eq-height">
                            <div class="col-sm-1">
                                <img src="/img/svg/geotag.svg" width="25" class="pull-left">
                            </div>
                            <div class="col-sm-11">
                                <span class="color-orange f-s-14 m-t-0-i m-b-0">{{ tx('re_town.name.' ~ object.re_town_id) }}, {{ tx('region.' ~ object.ReTown.re_region_id) }}</span>
                            </div>
                        </div>
                        <div class="row m-b-25">
                            <div class="col-xs-12">
                                <div>
                                    {{ tx("object.id") }}: {{ object.id }}
                                </div>
                                {% if object.firstline is 'Y' %}
                                    <div class="colorError">
                                        {{ tx('object.firstline') }}
                                    </div>
                                {% endif %}
                                <div>
                                    {{ tx('re_object.shortly.'~object.id, '') }}
                                </div>
                            </div>
                        </div>
                        <div class="row sm-row-eq-height md-row-eq-height lg-row-eq-height">
                            <div class="col-xs-12 col-sm-8">
                                <h3 class="font-weight-bold m-t-10 _text-nowrap">{{ object.getPrice() }} €</h3>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <a href="/object/{{ object.id }}" class="btn btn-primary full-width" target="_blank">{{ tx('see') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 m-t-25 m-b-25 p-l-10 p-r-10">
                        {% for attr in object.Attributes %}
                            {% if attr.value and attr.AttributeType %}
                                <span class="object-tag text-nowrap">
                                                {{ tx('re_attribute_types.name.' ~ attr.AttributeType.id) }}:
                                    {% if attr.AttributeType.valuetype is 'I' %}
                                        {{ attr.intvalue }} {{ attr.AttributeType.unit }}
                                    {% else %}
                                        {% if attr.AttributeType.controltype is 3 %}
                                            {% if attr.value is 'Y' %}
                                                {{ tx("yes") }}
                                            {% else %}
                                                {{ tx("no") }}
                                            {% endif %}
                                        {% else %}
                                            {{ attr.value }} {{ attr.AttributeType.unit }}
                                        {% endif %}
                                    {% endif %}
                                            </span>
                            {% endif %}
                        {% endfor %}
                    </div>
                </div>
            {% endfor %}
            {% if page.total_pages > 1 %}
                {% set paginatorArr = get_paginator_arr(page.current, page.total_pages) %}
                <div class="text-center">
                    <ul class="pagination m-t-0 m-b-0">
                        <li><a href="/{{ getParams }}&page={{ page.before }}"><span class="ti-angle-double-left"></span></a></li>
                        {% for p in paginatorArr %}
                            {% if p == 0 %}
                                <li class="pagination-dots"><span>...</span></li>
                            {% else %}
                                <li {% if p == page.current %} class="active"{% endif %}><a href="/{{ getParams }}&page={{ p }}">{{ p }} {% if p == page.current %}<span class="sr-only">(current)</span>{% endif %}</a></li>
                            {% endif %}
                        {% endfor %}
                        <li><a href="/{{ getParams }}&page={{ page.next }}"><span class="ti-angle-double-right"></span></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            {% endif %}
        {% else %}
            <div class="object-card p-l-10 p-t-20 p-r-10 p-b-20 m-b-30">
                {{ tx('error.objectnotfound') }}
            </div>
        {% endif %}
    </div>
{% else %}

    {% if page is defined and page.items|length > 0 %}
        <div class="row m-t-20">
            <div class="col-xs-12 col-sm-3 p-t-5">
                <select name="sort" onchange="sendFilterForm($(this).val())">
                    <option value="created"{% if sortBy is 'created' %} selected{% endif %}>{{ tx('list.sort.by.created') }}</option>
                    <option value="price"{% if sortBy is 'price' %} selected{% endif %}>{{ tx('list.sort.by.price') }}</option>
                </select>
            </div>
        {% if page is defined and page.total_pages > 1 %}
            {% set paginatorArr = get_paginator_arr(page.current, page.total_pages) %}
            <div class="col-xs-12 col-sm-6 text-center">
                <ul class="pagination m-b-0 m-t-10">
                    <li class="xs-hidden"><a href="/{{ getParams }}&page={{ page.before }}"><span class="ti-angle-double-left"></span></a></li>
                    {% for p in paginatorArr %}
                        {% if p == 0 %}
                            <li class="pagination-dots"><span>...</span></li>
                        {% else %}
                            <li {% if p == page.current %} class="active"{% endif %}><a href="/{{ getParams }}&page={{ p }}">{{ p }} {% if p == page.current %}<span class="sr-only">(current)</span>{% endif %}</a></li>
                        {% endif %}
                    {% endfor %}
                    <li class="xs-hidden"><a href="/{{ getParams }}&page={{ page.next }}"><span class="ti-angle-double-right"></span></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
        {% endif %}
            <div class="col-sm-3 xs-hidden"></div>
        </div>
    {% endif %}

    <div class="flex-container full-width m-l-0 m-r-0 m-t-25{% if page is not defined or page.total_pages < 2 %} xs-m-b-5 sm-m-b-5 m-b-35{% endif %}">
        {% if page is defined and page.items|length > 0 %}
            {% for object in page.items if object.re_town_id %}
                {{ partial("parts/object-card", ["object":object]) }}
            {% endfor %}
        {% else %}
            <div class="object-card p-l-10 p-t-20 p-r-10 p-b-20 m-b-15">
                {{ tx('error.objectnotfound') }}
            </div>
        {% endif %}
    </div>
    {% if page is defined and page.total_pages > 1 %}
        {% set paginatorArr = get_paginator_arr(page.current, page.total_pages) %}
        <div class="text-center xs-m-b-5 sm-m-b-5 m-b-35">
            <ul class="pagination m-t-15 m-b-5">
                <li><a href="/{{ getParams }}&page={{ page.before }}"><span class="ti-angle-double-left"></span></a></li>
                {% for p in paginatorArr %}
                    {% if p == 0 %}
                        <li class="pagination-dots"><span>...</span></li>
                    {% else %}
                        <li {% if p == page.current %} class="active"{% endif %}><a href="/{{ getParams }}&page={{ p }}">{{ p }} {% if p == page.current %}<span class="sr-only">(current)</span>{% endif %}</a></li>
                    {% endif %}
                {% endfor %}
                <li><a href="/{{ getParams }}&page={{ page.next }}"><span class="ti-angle-double-right"></span></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
    {% endif %}

{% endif %}
