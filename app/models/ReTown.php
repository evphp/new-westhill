<?php


class ReTown extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $ispopular;

    /**
     *
     * @var integer
     */
    public $re_region_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema(\Phalcon\DI::getDefault()->getShared('config')->database->dbname);
        $this->setSource("re_town");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 're_town';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ReTown[]|ReTown|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ReTown|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function getSortedArray()
    {
        $DI = \Phalcon\DI::getDefault();
        $tx = $DI->getShared('tx');

        $reTownObj = self::find(['order' => 'name']);
        $reTownArr = [];
        foreach ($reTownObj AS $val) {
            $txName = $tx->query('re_town.name.'.$val->id, $val->name);
            $reTownArr[$txName] = ['id' => $val->id, 'name' => $txName, 'region_id' => $val->re_region_id ];
        }
        ksort($reTownArr);
        $sortedReTownArr = [];
        foreach ($reTownArr AS $v) {
            $sortedReTownArr[$v['id']] = [
                'id' => $v['id'],
                'name' => $v['name'],
                'region_id' => $v['region_id']
            ];
        }
        return $sortedReTownArr;
    }
}
