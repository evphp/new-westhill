<?php

class ReAttributeTypes extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $re_types_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $unit;

    /**
     *
     * @var integer
     */
    public $display;

    /**
     *
     * @var integer
     */
    public $translateble;

    /**
     *
     * @var integer
     */
    public $controltype;

    /**
     *
     * @var integer
     */
    public $weight;

    /**
     *
     * @var string
     */
    public $valuetype;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema(\Phalcon\DI::getDefault()->getShared('config')->database->dbname);
        $this->setSource('re_attribute_types');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 're_attribute_types';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ReAttributeTypes[]|ReAttributeTypes|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ReAttributeTypes|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
