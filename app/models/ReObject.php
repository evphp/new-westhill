<?php


class ReObject extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $manager_id;

    /**
     *
     * @var integer
     */
    public $re_town_id;

    /**
     *
     * @var integer
     */
    public $re_types_id;

    /**
     *
     * @var integer
     */
    public $re_deal_type_id;

    /**
     *
     * @var integer
     */
    public $re_rental_frequency_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var double
     */
    public $price;

    /**
     *
     * @var double
     */
    public $bestprice;

    /**
     *
     * @var double
     */
    public $minprice;

    /**
     *
     * @var double
     */
    public $maxprice;

    /**
     *
     * @var double
     */
    public $price_for_sorting;

    /**
     *
     * @var string
     */
    public $x;

    /**
     *
     * @var string
     */
    public $y;

    /**
     *
     * @var integer
     */
    public $zoom;

    /**
     *
     * @var string
     */
    public $active;

    /**
     *
     * @var string
     */
    public $comision;

    /**
     *
     * @var string
     */
    public $saler;

    /**
     *
     * @var string
     */
    public $saler_name;

    /**
     *
     * @var string
     */
    public $saler_email;

    /**
     *
     * @var string
     */
    public $saler_email_template;

    /**
     *
     * @var string
     */
    public $saler_email_sender;

    /**
     *
     * @var integer
     */
    public $saler_language_id;

    /**
     *
     * @var string
     */
    public $next_price_update_date;

    /**
     *
     * @var string
     */
    public $reserved;

    /**
     *
     * @var integer
     */
    public $re_image_id;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $istop;

    /**
     *
     * @var integer
     */
    public $viewed;

    /**
     *
     * @var string
     */
    public $firstline;

    /**
     *
     * @var string
     */
    public $accepted;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema(\Phalcon\DI::getDefault()->getShared('config')->database->dbname);
        $this->setSource('re_object');
        $this->hasMany('id', 'ReImages', 're_object_id', ['alias' => 'Images', 'params' => ['order' => 'id ASC']]); //, 'conditions' => 'status = "1"'
        $this->hasMany('id', 'ReAttributes', 're_object_id', ['alias' => 'Attributes', 'params' => ['order' => 'id ASC']]);
        $this->hasOne('re_image_id', 'ReImages', 'id', ['alias' => 'Image']);
        $this->hasOne('re_town_id', 'ReTown', 'id');
        $this->hasOne('re_types_id', 'ReTypes', 'id');
        $this->hasOne('re_deal_type_id', 'ReDealType', 'id');
//        $this->hasOne('re_rental_frequency_id', 'ReRentalFrequency', 'id');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 're_object';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ReObject[]|ReObject|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ReObject|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function getPrice()
    {
        $decimalSeparator = '.';
        $groupSeparator = ',';
        
        if($this->bestprice !== null && $this->bestprice > 0){
            return number_format($this->bestprice,0 ,$decimalSeparator , $groupSeparator);
        } else if ($this->minprice !== null && $this->maxprice !== null && $this->maxprice > 0 && $this->minprice > 0){
            return number_format($this->minprice ,0 ,$decimalSeparator , $groupSeparator) .
                '&nbsp;-&nbsp;' . number_format($this->maxprice ,0 ,$decimalSeparator , $groupSeparator);
        } else {
            return number_format($this->price ,0 ,$decimalSeparator , $groupSeparator);
        }
    }

    public function additionalHtmlFromCSV($default = null)
    {
        $csvText = $this->getDI()->get('tx')->query('re_object.additional.' . $this->id, '');

        if ($csvText) {
            $textArr = explode("\n", $csvText);

            $i = 0;
            $tmpArr = [];
            foreach ($textArr AS $currStr) {
                $currStr = trim($currStr);
                if (!$currStr) {
                    $i++;
                    $tmpArr[$i] = '';
                    continue;
                }
                $strArr = explode(',', $currStr);
                if (count($tmpArr) && (!is_array($tmpArr[$i]) || count($strArr) !== count($tmpArr[$i]))) {
                    $i++;
                }
                foreach ($strArr AS $k => $v) {
                    $tmpArr[$i][$k][] = $v;
                }
            }

            $html = '';
            foreach ($tmpArr AS $subArr) {
                if (is_array($subArr) && count($subArr)) {
                    $html .= '<div>';
                    foreach ($subArr AS $val) {
                        $html .= '<div style="display:inline-block"><ul><li>';
                        $html .= implode('</li><li>', $val);
                        $html .= '</li></ul></div>';
                    }
                    $html .= '</div>';
                }
            }
            return $html;
        }

        return $default !== null ? $default : ''; // '$'.$textid.'$';
    }
}
