<?php

namespace Components\Forms;

use \Phalcon\Forms\Form;
use \Phalcon\Forms\Element\Text;
use \Phalcon\Forms\Element\Check;
use \Phalcon\Forms\Element\Select;

use \Phalcon\Validation\Validator\Between;
use \Phalcon\Validation\Validator\Numericality;
use \Phalcon\Validation\Validator\StringLength;

class FilterForm extends Form
{
    public function initialize() // $entity = null, $options = array()
    {
        $entity = $this->getEntity();
        $options = $this->getUserOptions();


        // re_deal_type
        $reDealTypesObj = \ReDealType::find(['order' => 'weight']);
        $reDealTypesArr = [];
        foreach ($reDealTypesObj AS $val) {
            $reDealTypesArr[$val->id] = $this->tx->query('re_deal_type.name.'.$val->id, $val->name);
        }
        $reDealTypes = new Select(
            'deal',
            $reDealTypesArr,
            [
                'useEmpty' => false, // set default option
                'emptyText' => $this->tx->query('menu.any'),
                //'using' => ['id','name'],
                'class' => 'darkSelect',
                'data-placeholder' => '',
                'maxlength' => '2'
                //'value' => 'RU'
            ]
        );
        if (isset($entity->dealType) && $entity->dealType !== '') {
            $reDealTypes->addValidators([
                new Between([
                    'allowEmpty' => false,
                    'minimum' => 1,
                    'maximum' => 1000,
                    'message' => $this->tx->query('errormsg_deal_type_is_wrong', 'Deal type is wrong'),
                ]),
                new Numericality([
                    'allowEmpty' => false,
                    'message' => $this->tx->query('errormsg_deal_type_is_not_numeric', 'Deal type is not numeric'),
                ]),
            ]);
        }
        $reDealTypes->setFilters(['trim', 'string','int!']);
        $this->add($reDealTypes);



        // re_types
        $reTypesObj = \ReTypes::find(['order' => 'weight']);
        $reTypesArr = [];
        foreach ($reTypesObj AS $val) {
            $reTypesArr[$val->id] = $this->tx->query('re_types.name.'.$val->id);
        }
        $reTypes = new Select(
            'type',
            $reTypesArr,
            [
                'useEmpty' => true, // set default option
                'emptyText' => $this->tx->query('menu.any'),
                //'using' => ['id','name'],
                'class' => 'darkSelect',
                'data-placeholder' => '',
                'maxlength' => '2'
                //'value' => 'RU'
            ]
        );
        if (isset($entity->type) && $entity->type !== '') {
            $reTypes->addValidators([
                new Between([
                    'allowEmpty' => false,
                    'minimum' => 1,
                    'maximum' => 1000000000,
                    'message' => $this->tx->query('errormsg_type_is_wrong', 'Type is wrong'),
                ]),
                new Numericality([
                    'allowEmpty' => false,
                    'message' => $this->tx->query('errormsg_type_is_not_numeric', 'Type is not numeric'),
                ]),
            ]);
        }
        $reTypes->setFilters(['trim', 'string','int!']);
        $this->add($reTypes);


        // re_region
        $reRegionObj = \ReRegion::find(['order' => 'name']);
        $reRegionArr = [];
        foreach ($reRegionObj AS $val) {
            $txName = $this->tx->query('region.'.$val->id, $val->name);
            $reRegionArr[$txName] = ['id' => $val->id, 'name' => $txName];
        }
        ksort($reRegionArr);
        $sortedReRegionArr = [];
        foreach ($reRegionArr AS $v) {
            $sortedReRegionArr[$v['id']] = $v['name'];
        }
        $reRegions = new Select(
            'region',
            $sortedReRegionArr,
            [
                'useEmpty' => true, // set default option
                'emptyText' => $this->tx->query('menu.any'),
                'class' => 'darkSelect',
                'onchange' => 'filterTowns()',
                'data-placeholder' => '',
                'maxlength' => '2'
                //'value' => 'RU'
            ]
        );
        if (isset($entity->region) && $entity->region !== '') {
            $reRegions->addValidators([
                new Between([
                    'allowEmpty' => false,
                    'minimum' => 1,
                    'maximum' => 1000000000,
                    'message' => $this->tx->query('errormsg_region_is_wrong', 'Region is wrong'),
                ]),
                new Numericality([
                    'allowEmpty' => false,
                    'message' => $this->tx->query('errormsg_region_is_not_numeric', 'Region is not numeric'),
                ]),
            ]);
        }
        $reRegions->setFilters(['trim', 'string','int!']);
        $this->add($reRegions);

        // re_town
        $regionId = (isset($entity->region) && $entity->region !== '') ? $entity->region : 0;
        $sortedTownArr = isset($options['towns']) ? $options['towns'] : \ReTown::getSortedArray();
        $townsList = [];
        foreach ($sortedTownArr AS $val) {
            if ($regionId && $regionId != $val['region_id']) continue;
            $townsList[$val['id']] = $val['name'];
        }

        $reTowns = new Select(
            'town',
            $townsList,
            [
                'useEmpty' => true, // set default option
                'emptyText' => $this->tx->query('menu.any'),
                'class' => 'darkSelect',
                'data-placeholder' => '',
                'maxlength' => '2'
                //'value' => 'RU'
            ]
        );
        if (isset($entity->town) && $entity->town !== '') {
            $reTowns->addValidators([
                new Between([
                    'allowEmpty' => false,
                    'minimum' => 1,
                    'maximum' => 1000000000,
                    'message' => $this->tx->query('errormsg_town_is_wrong', 'Town is wrong'),
                ]),
                new Numericality([
                    'allowEmpty' => false,
                    'message' => $this->tx->query('errormsg_town_is_not_numeric', 'Town is not numeric'),
                ]),
            ]);
        }
        $reTowns->setFilters(['trim', 'string','int!']);
        $this->add($reTowns);


        // object id
        $objectId = new Text('object', [
            'class' => 'form-control',
            'placeholder' => $this->tx->query('menu.jump.text'),
            'maxlength' => '10'
        ]);
        if (isset($entity->object) && $entity->object !== '') {
            $objectId->addValidators([
                new Between([
                    'allowEmpty' => false,
                    'minimum' => 1,
                    'maximum' => 1000000000,
                    'message' => $this->tx->query('errormsg_object_is_wrong', 'Object is wrong'),
                ]),
                new Numericality([
                    'allowEmpty' => false,
                    'message' => $this->tx->query('errormsg_object_is_not_numeric', 'Object is not numeric'),
                ]),
            ]);
        }
        $objectId->setFilters(['trim', 'string','int!']);
        $this->add($objectId);


        // min price
        $minPrice = new Text('minprice', [
            'class' => 'form-control',
            'placeholder' => $this->tx->query('min'),
            'maxlength' => '12'
        ]);
        if (isset($entity->minprice) && $entity->minprice !== '') {
            $minPrice->addValidators([
                new Between([
                    'allowEmpty' => false,
                    'minimum' => 10,
                    'maximum' => 1000000000,
                    'message' => $this->tx->query('errormsg_minprice_is_wrong', 'Minimum price is wrong'),
                ]),
                new Numericality([
                    'allowEmpty' => false,
                    'message' => $this->tx->query('errormsg_minprice_is_not_numeric', 'Minimum price is not numeric'),
                ]),
            ]);
        }
        $minPrice->setFilters(['trim', 'string','int!']);
        $this->add($minPrice);


        // max price
        $maxPrice = new Text('maxprice', [
            'class' => 'form-control',
            'placeholder' => $this->tx->query('max'),
            'maxlength' => '12'
        ]);
        if (isset($entity->maxprice) && $entity->maxprice !== '') {
            $maxPrice->addValidators([
                new Between([
                    'allowEmpty' => false,
                    'minimum' => 10,
                    'maximum' => 1000000000,
                    'message' => $this->tx->query('errormsg_maxprice_is_wrong', 'Maximum price is wrong'),
                ]),
                new Numericality([
                    'allowEmpty' => false,
                    'message' => $this->tx->query('errormsg_maxprice_is_not_numeric', 'Maximum price is not numeric'),
                ]),
            ]);
        }
        $maxPrice->setFilters(['trim', 'string','int!']);
        $this->add($maxPrice);

        // first line
        $firstline = new Check('firstline', [
            'value' => '1',
            'class' => 'form-check-input',
            'maxlength' => '1'
        ]);
        if (isset($entity->firstline) && $entity->firstline !== '') {
            $firstline->addValidators([
                new Between([
                    'allowEmpty' => false,
                    'minimum' => 0,
                    'maximum' => 1,
                    'message' => $this->tx->query('errormsg_firstline_is_wrong', 'Urgent sale is wrong'),
                ]),
                new Numericality([
                    'allowEmpty' => false,
                    'message' => $this->tx->query('errormsg_firstline_is_not_numeric', 'Urgent sale is not numeric'),
                ]),
            ]);
        }
        $firstline->setFilters(['trim', 'string','int!']);
        $this->add($firstline);

    }
}
