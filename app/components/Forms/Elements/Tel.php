<?php
namespace Components\Forms\Elements;

use Phalcon\Forms\Element;


class Tel extends Element
{
    public function render($attributes = null)
    {
        if(!$attributes) {
            $attributes = $this->getAttributes();
        }

        $attributesStr = implode(' ', array_map(
            function ($v, $k) { return $k.'="'.$v.'"'; },
            $attributes,
            array_keys($attributes)
        ));

        $html = '<input type="tel" id="'.$this->getName().'" name="'.$this->getName().'" '.$attributesStr.($this->getValue()? ' value="'.$this->getValue().'"': '').'>';

        return $html;
    }
}