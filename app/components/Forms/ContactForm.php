<?php

namespace Components\Forms;

use \Phalcon\Forms\Form;
use \Phalcon\Forms\Element\Text;
use \Phalcon\Forms\Element\TextArea;

use \Phalcon\Validation\Validator\Between;
use \Phalcon\Validation\Validator\Numericality;
use \Phalcon\Validation\Validator\PresenceOf;
use \Phalcon\Validation\Validator\StringLength;
use \Phalcon\Validation\Validator\Email as EmailValidator;

class ContactForm extends Form
{
    public function initialize() // $entity = null, $options = array()
    {

        // name
        $name = new Text('cfName', [
            'class' => 'form-control bg-white',
            'required' => 'true',
            'maxlength' => '50',
            //'placeholder' => $this->tx->query('placeholder_name'),
        ]);
        $name->addValidators([
            new PresenceOf([
                'message' => $this->tx->query('errormsg_name_is_required'),
                'cancelOnFail' => true
            ]),
            new StringLength([
                'min' => 3,
                'messageMinimum' => $this->tx->query('errormsg_name_is_too_short'),
            ])
        ]);
        $name->setFilters(['trim', 'string']);
        $this->add($name);


        // email
        $email = new Text('cfEmail', [
            'class' => 'form-control bg-white',
            'maxlength' => '50',
            //'placeholder' => $this->tx->query('placeholder_email'),
            'required' => 'true',
            //'readonly' => 'readonly'
        ]);
        $email->addValidators([
            new PresenceOf([
                'message' => $this->tx->query('errormsg_email_is_required', 'Email is required'),
                'cancelOnFail' => true
            ]),
            new EmailValidator([
                'message' => $this->tx->query('errormsg_email_is_not_valid', 'Email is not valid'),
            ])
        ]);
        $email->setFilters(['trim', 'string']);
        $this->add($email);


        // message
        $message = new TextArea('cfMessage', [
            'class' => 'form-control bg-white',
            'required' => 'true',
            //'placeholder' => $this->tx->query('placeholder_message'),
        ]);
        $message->addValidators([
            new StringLength([
                'allowEmpty' => true,
                'min' => 10,
                'messageMinimum' => $this->tx->query('errormsg_message_is_too_short', 'Message is too short'),
            ])
        ]);
        $message->setFilters(['trim', 'string']);
        $this->add($message);

        // object id
        $objectId = new Text('object', [
            'class' => 'form-control',
            'maxlength' => '10'
        ]);
        $objectId->addValidators([
            new Between([
                'allowEmpty' => true,
                'minimum' => 1,
                'maximum' => 1000000000,
                'message' => $this->tx->query('errormsg_object_is_wrong', 'Object is wrong'),
            ]),
            new Numericality([
                'allowEmpty' => true,
                'message' => $this->tx->query('errormsg_object_is_not_numeric', 'Object is not numeric'),
            ]),
        ]);
        $objectId->setFilters(['int!']);
        $this->add($objectId);
    }
}
