<?php
namespace Components\Validators;

use Phalcon\Validation;
use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;
use Phalcon\Validation\Message;


class AgeValidator extends Validator implements ValidatorInterface
{
    public function __construct($options = null) {

        parent::__construct($options);
        $this->setOption("cancelOnFail",true);
    }

    /**
     * Executes the validation
     *
     * @param mixed $validation
     * @param string $attribute
     * @return bool
     */
    public function validate(Validation $validator, $attribute)
    {
        $birthday = $validator->getValue($attribute);

        if ($birthday && mb_strlen($birthday) > 1) {

            // calculate age
            $d1 = new \DateTime();
            $d1->setTimestamp(strtotime($birthday));
            $d2 = new \DateTime();
            $d2->setTimestamp(time());
            $age = $d2->diff( $d1 );

            if ($age->y < 18 || $d1 > $d2) {
                $message = $this->getOption('message');
                if (!$message) {
                    //message was not provided, so set some default
                    $message = "You have to be at least 18 years old";
                }
                $validator->appendMessage(new Message($message, $attribute));
                return false;
            }
        }

        return true;
    }
}