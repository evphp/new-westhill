<?php
namespace Components\Validators;

use Phalcon\Validation;
use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;
use Phalcon\Validation\Message;


class MultipleValidator extends Validator implements ValidatorInterface
{
    public function __construct($options = null) {

        parent::__construct($options);
        $this->setOption("cancelOnFail", true);
    }

    /**
     * Executes the validation
     *
     * @param mixed $validation
     * @param string $attribute
     * @return bool
     */
    public function validate(Validation $validator, $attribute)
    {
        $n = (integer)$validator->getValue($attribute);
        $number = $this->getOption('number');

        if ($n % $number != 0) {
            $message = $this->getOption('message');
            if (!$message) {
                //message was not provided, so set some default
                $message = 'You must enter a multiple of '.$number.'.';
            }
            $validator->appendMessage(new Message($message, $attribute));
            return false;
        }

        return true;
    }
}