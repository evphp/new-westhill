<?php
namespace Components\Validators;

use Phalcon\Validation;
use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;
use Phalcon\Validation\Message;


class PhoneValidator extends Validator implements ValidatorInterface
{
    public function __construct($options = null) {

        parent::__construct($options);
        $this->setOption("cancelOnFail",true);
    }

    /**
     * Executes the validation
     *
     * @param mixed $validation
     * @param string $attribute
     * @return bool
     */
    public function validate(Validation $validator, $attribute)
    {
        $phone = $validator->getValue($attribute);

        if ($phone && mb_strlen($phone) > 1) {

            $DI = \Phalcon\DI::getDefault();
            $userId = $DI->getShared('session')->get('userId');

            $phoneLast3 = mb_substr($phone,-3, 3);
            $userPhoneCheck = \User::findFirst(array(
                'conditions' => "phoneHash=?1 AND siteId=?2",
                'bind' => array(1 => md5($phone)."".$phoneLast3, 2 => $DI->get('config')->countryInfo->countryId)
            ));

            if ($userPhoneCheck && $userPhoneCheck->userId != $userId) {
                $message = $this->getOption('message');
                if (!$message) {
                    //message was not provided, so set some default
                    $message = "Phone number is already in use";
                }
                $validator->appendMessage(new Message($message, $attribute));

                return false;
            } else {
                if ($DI->getShared('session')->get('phoneNumber') != $phone ||
                    !$DI->getShared('session')->has('X'.$phone) ||
                    !$DI->getShared('session')->get('X'.$phone))
                {
                    $messageConfirm = $this->getOption('messageConfirm');
                    if (!$messageConfirm) {
                        //message was not provided, so set some default
                        $messageConfirm = "Phone number is not confirmed";
                    }
                    $validator->appendMessage(new Message($messageConfirm, $attribute));

                    return false;
                }
            }
        }

        return true;
    }
}