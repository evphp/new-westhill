<?php

namespace Library\Texts\Adapter;

use Phalcon\Translate\Adapter;
use Phalcon\Translate\AdapterInterface;
use Phalcon\Translate\Exception;

class Database extends Adapter implements AdapterInterface
{
    /**
     * @var array
     */
    protected $options;

    /**
     * Class constructor.
     *
     * @param  array $options
     * @throws \Phalcon\Translate\Exception
     */
    public function __construct($options)
    {
        if (!isset($options['db'])) {
            throw new Exception("Parameter 'db' is required");
        }

        if (!isset($options['table'])) {
            throw new Exception("Parameter 'table' is required");
        }

        if (!isset($options['language'])) {
            throw new Exception("Parameter 'language' is required");
        }

        $this->options = $options;
    }

    /**
     * Returns text or text id or default.
     *
     * @param  string $textId
     * @param  string $default
     * @param  boolean|string $lang
     * @return string
     */
    public function query($textId, $default = null, $lang = false)
    {
        $options = $this->options;
        $language = $options['language'];

        $DI = \Phalcon\DI::getDefault();
        if ($lang) { // lang - TRUE or string (en, ru, ...)
            if (gettype($lang) === 'string' && in_array($lang, $DI->getShared('config')->application->availableLanguages->toArray())) {
                $language = $lang;
            } else {
                $language = $DI->getShared('config')->application->defaultLang;
            }
        }
        $languageId = $DI->getShared('config')->application->enabledLanguages[$language]['id'];

        $texts = $options['db']->fetchOne(
            sprintf(
                "SELECT `text` FROM %s WHERE textid = '%s' AND languages_id = ?",
                $options['table'], $textId),
            null,
            array($languageId)
        );
        if (!$texts) {
            return $default !== null ? $default : $textId;
        }
        return $texts['text'];
    }

    public function replace($textId, $arr, $default = null, $lang = false)
    {
        $text = $this->query($textId, $default, $lang);

        foreach($arr as $key => $val){
            $text = str_replace($key , $val , $text);
        }
        return $text;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string  $textId
     * @return boolean
     */
    public function exists($textId)
    {
        $options = $this->options;

        $DI = \Phalcon\DI::getDefault();
        $language = $DI->getShared('config')->application->defaultLang;
        $languageId = $DI->getShared('config')->application->enabledLanguages[$language]['id'];

        $exists = $options['db']->fetchOne(
            sprintf(
                "SELECT COUNT(*) FROM %s WHERE textid = '%s' AND languages_id = ?0",
                $options['table'], $textId),
            null,
            array($languageId)
        );

        return $exists[0] > 0;
    }
}
