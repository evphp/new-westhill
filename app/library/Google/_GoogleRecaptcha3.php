<?php

namespace Library\Google;

/**
 * https://www.google.com/recaptcha/intro/v3.html
 */
class GoogleRecaptcha3
{
    /**
     * Return Google scoring (Turing test)
     * @param string $token
     * @return \stdClass object
     */
    public static function getScore($token)
    {
        $result = null;

        $DI = \Phalcon\DI::getDefault();
        $params = [
            'secret' => $DI->getShared('config')->googleRecaptcha->v3->secret_key,
            'response' => $token,
            'remoteip' => $_SERVER['REMOTE_ADDR'] // optional
        ];

        $ch = curl_init($DI->getShared('config')->googleRecaptcha->url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);
        if(!empty($response))  {
            $result = json_decode($response);
        }

        return $result;
    }

    /**
     * Check user is human or bot
     * @param string $token
     * @param string $action
     * @param float $scoreThreshold (0-1)
     * @return boolean
     */
    public static function isHuman($token, $action, $scoreThreshold = 0.3)
    {
        $isHuman = false;
        $recaptchaScore = self::getScore($token);
        if ($recaptchaScore && $recaptchaScore->success && $recaptchaScore->action === $action && $recaptchaScore->score >= $scoreThreshold) {
            $isHuman = true;
        }
        return $isHuman;
    }
}