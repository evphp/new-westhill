<?php

namespace Library\Google;

/**
 * https://developers.google.com/recaptcha/docs/v3
 */
class GoogleRecaptcha2
{
    /**
     * Return Google verify
     * @param string $captchaResponse
     * @return \stdClass object
     */
    public static function verify($captchaResponse)
    {
        $result = null;

        $DI = \Phalcon\DI::getDefault();
        $params = [
            'secret' => $DI->getShared('config')->googleRecaptcha->v2->secret_key,
            'response' => $captchaResponse,
            'remoteip' => $_SERVER['REMOTE_ADDR'] // optional
        ];

        $ch = curl_init($DI->getShared('config')->googleRecaptcha->url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Disable SSL verification
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        if(!empty($response))  {
            $result = json_decode($response);
        }

        return $result;
    }

    /**
     * Check user is human or bot
     * @param string $captchaResponse
     * @return boolean
     */
    public static function isHuman($captchaResponse)
    {
        $isHuman = false;
        $verifyResult = self::verify($captchaResponse);

        if ($verifyResult && $verifyResult->success) {
            $isHuman = true;
        }
        return $isHuman;
    }
}