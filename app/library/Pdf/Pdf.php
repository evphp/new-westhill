<?php

namespace Library\Pdf;

define('FPDF_FONTPATH','fonts/');
require_once 'config/lang/rus.php';
use TCPDF;
require_once 'tcpdf.php';
require_once 'unicode_data.php';
require_once 'htmlcolors.php';

class Pdf extends TCPDF
{
    public function printTest()
    {
        // set document information
        $this->SetCreator(PDF_CREATOR);
        $this->SetAuthor('Nicola Asuni');
        $this->SetTitle('TCPDF Example 001');
        $this->SetSubject('TCPDF Tutorial');
        $this->SetKeywords('TCPDF, PDF, example, test, guide');

//        // set default header data
//        $this->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
//        //$this->setFooterData(array(0,64,0), array(0,64,128));
//
//        // set header and footer fonts
//        $this->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//        $this->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//
//        // set default monospaced font
//        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
//
//        // set margins
//        $this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//        $this->SetHeaderMargin(PDF_MARGIN_HEADER);
//        $this->SetFooterMargin(PDF_MARGIN_FOOTER);
//
//        // set auto page breaks
//        $this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
//
//        // set image scale factor
//        $this->setImageScale(PDF_IMAGE_SCALE_RATIO);
//
//        // set some language-dependent strings (optional)
//        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
//            require_once(dirname(__FILE__).'/lang/eng.php');
//            $this->setLanguageArray($l);
//        }
//
//        // set default font subsetting mode
//        //$this->setFontSubsetting(true);
//
//        // Set font
//        // dejavusans is a UTF-8 Unicode font, if you only need to
//        // print standard ASCII chars, you can use core fonts like
//        // helvetica or times to reduce file size.
//        //$this->SetFont('dejavusans', '', 14, '', true);
//
//        // Add a page
//        // This method has several options, check the source code documentation for more information.
        $this->AddPage();
//
//        // set text shadow effect
//        //$this->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

        // Set some content to print
        $html = <<<EOD
        <h1>Welcome to Maiami</h1>
EOD;

        // Print text using writeHTMLCell()
        $this->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        //$this->writeHTML('<strong>hellow world</strong>');

        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $this->Output('example_001.pdf', 'I');

    }

    public function test($id)
    {
        var_dump('HelloWorld!!!', $id);
    }
}
