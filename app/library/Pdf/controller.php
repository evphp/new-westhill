<?php
define('FPDF_FONTPATH','fonts/');
require_once('config/lang/rus.php');
require_once('tcpdf.php');
require_once('unicode_data.php');
require_once('htmlcolors.php');

$selfcom  = $commanager->getWorkingComponent();
$data = array();

$id = $_REQUEST['action'];
if(!is_numeric($id)){
	$commanager->addError('error.objectnotfound');
	$selfcom->name='default';
	$selfcom->_runController();
	return;
}
$obj = $loader->load('re_object' , $id);

if(!isset($obj) || ($obj->active !='Y' && !isset($_REQUEST['admin']))){
	$commanager->addError('error.objectnotfound');
	$selfcom->name='default';
	$selfcom->_runController();
	return;
}

$loader->sqlUpdate("update ".re_object::$_table . " set viewed  = " . ($obj->viewed + 1) ." where id = " . $obj->id );


$data['object'] = $obj;
$data['town'] = $loader->load('re_town', $obj->re_town_id);
$data['images'] = $loader->loadlist('re_images', " re_object_id = '".$loader->SLQFilter($id)."' and pdf = 'Y'");
if(count($data['images']) == 0){
	$data['images'] = $loader->loadlist('re_images', " re_object_id = '".$loader->SLQFilter($id)."'");
}
$attrtypes = $loader->loadlist('re_attribute_types', " display <> 2 and re_types_id = ".$loader->SLQFilter($obj->re_types_id),'weight');
$selfcom->data['attributes'] = array();
loadValues($selfcom, $id , $obj->re_types_id, $attrtypes);
$values = $selfcom->data['attributes'];

function loadValues($com , $id , $types_id , $types ){
	global $loader;

	$values = $loader->loadlist('re_attributes'," re_object_id = $id and re_types_id = $types_id ");
	foreach($values as $att){
		$com->data['attributes'][$att->re_attribute_types_id] = $att;
	}

}
$font = 'dejavuserifcondensed';
global $l;

class PDF extends TCPDF{
function Footer()
{
if($_REQUEST['salerinfo'] != 'true'){
	$this->SetLineWidth(1);
	$this->SetDrawColor(200);
	$x1 = 10;
	$y1 = $this->getPageHeight() - 65;
	$x2 = $this->getPageWidth()-20;
	$y2 = $y1;
	$this->Line($x1, $y1,$x2,$y2);
	$this->Image('http://'.$_SERVER['HTTP_HOST'].'/images/logo_for_print.jpg' ,$x1 , $y1 + 5 , 50 );
    $this->SetY(-60);
	$this->Cell(0,3 ,'', 0 , 1);
    $this->SetFont('Helvetica','',8);
	$this->Cell(50,9 ,'' );
	$this->Cell(180,9 ,'Estate Agency West Hill' );
	$this->Cell(180,9 ,'Phone numbers:' );
	$this->Cell(180,9 ,'montenegro@westhill.ru' );
	$this->Ln();
	$this->Cell(50,9 ,'' );
	$this->Cell(180,9 ,'Lazi BB, 85310 Budva' );
	$this->Cell(180,9 ,'+382 69231168' );
	$this->Cell(180,9 ,'www.westhill.ru' );
	$this->Ln();
	$this->Cell(50,9 ,'' );
	$this->Cell(180,9 ,'MONTENEGRO' );
	$this->Cell(180,9 ,'+382 68507585' );
	$this->Cell(180,9 ,'' );
	}
}
function Header(){
if( $_REQUEST['salerinfo'] != 'true'){
	$this->SetFont('Helvetica','',7);
	$this->SetY(10);
	$this->Cell(0,9 ,'Estate Agency West Hill   |   ' .
	'http://'.$_SERVER['HTTP_HOST'] . '/' . lng(). '/object/'.$_REQUEST['action'] . '/  |  ' . date('d.m.Y H:i') );
	}
}
}

function conv($s){
	$s = strip_tags($s);
	$s = str_replace('&nbsp;' , ' ' , $s);
	return $s;
}
$pdf=new PDF('P','pt','A4', true, 'UTF-8', true);


$place = conv(tx('place').': ' .tx(re_town::$name_text_id . $obj->re_town_id));
$price = $obj->getPrice();
if ($price == '0' || $price == 0){
	$price= tx('list.negotiatedprice');
}else{
	$price .=' EUR';
}
$price = conv(tx('price').': ' .$price  );
$title = tx(re_object::$name_text_id . $obj->id);
$title = conv($title);

$objid = conv(tx('object.id').': ' .$obj->id);

$pdf->setFooterMargin(70);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont($font, '', 12);

// add a page
$pdf->AddPage();
$pdf->SetTitle($title);
$pdf->SetAuthor('West Hill');
$pdf->SetFont($font,'',16);
$pdf->SetTextColor(1,42,74);
$pdf->Cell(275,20,$title);
$pdf->Cell(0,20,$price,0,0,'R');
$pdf->Ln();
$pdf->SetFont($font,'',12);
$pdf->Cell(0,16,$objid);
$pdf->Ln();
$pdf->Cell(0,16,$place);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont($font,'',10);

$cnt = 0;
foreach($attrtypes as $type){
$isnumber = $type->valuetype == re_attribute_types::$valuetype_int;
$value = isset($values[$type->id]) ? ( $isnumber? $values[$type->id]->intvalue : $values[$type->id]->value) : '';
if($type->controltype == re_attribute_types::$control_checkbox ){
	$value = $value == 'Y' ? tx('yes') : tx('no');
}
if($type->display == 0 || (isset($value) && strlen(trim($value)) > 0 ) ){
	$a = tx(re_attribute_types::$name_text_id . $type->id) . ": "  ;
	$b =  $value. ' ' . $type->unit  ;
	$a = conv($a);
	$b = conv($b);
	$pdf->Cell(255,16,$a .$b, 0, $cnt++ %2 != 0 ? 1 : 0 );
}
}
//$pdf->SetY(150);
$pdf->Cell(0,0," ",0,2);
$pdf->Ln();
$desc = conv(tx(re_object::$description_text_id . $obj->id, '  '));
$pdf->SetFont($font,'',10);
$pdf->MultiCell(0,16 , $desc, 0 , 'L' );

$imgs = $data['images'];
$images = array();
$images[] = $imgs[$obj->re_image_id];
foreach($imgs as $im){
	if ($im->id != $obj->re_image_id){
		$images[] = $im;
	}
	if(count($images)>=6) break;
}
$cnt = 0;
$margin = 35;
$offset = $pdf->GetY() + 5 ;
foreach($images as $im){

if($im->pdf_image || $im->mid_image){
$pdf->Image('http://'.$_SERVER['HTTP_HOST'].(isset($im->pdf_image) ? $im->pdf_image : $im->mid_image) , 
$margin + ($cnt % 2 == 0 ? 0 : 255 ) , 
($cnt % 2 == 0 ? $cnt / 2 : ($cnt - 1 )/ 2) * 154 + $offset , 250 , 149);
if( $cnt % 2 != 0 ){
$pdf->Ln();
}
$cnt++;
}
}




$pdf->Output('object_'.$obj->id.'.pdf', 'D');
$loader->releaseConnection();
exit(0);
?>