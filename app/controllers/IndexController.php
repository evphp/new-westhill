<?php

use Components\Forms\FilterForm;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class IndexController extends ControllerBase
{
    public function indexAction()
    {
        Phalcon\Tag::setTitle($this->tx->query('default.title'));

//        $tokenStyleVer = filemtime(BASE_PATH . '/css/magnific-popup.css');
//        $tokenJSVer = filemtime(BASE_PATH . '/public/js/module/token.js');
        $headerCss[] = '/css/magnific-popup.css?v=' . filemtime(BASE_PATH . '/public/css/magnific-popup.css');
        $headerCss[] = '/css/animate.css?v=' . filemtime(BASE_PATH . '/public/css/animate.css');
        $headerCss[] = '/css/select2.min.css?v=' . filemtime(BASE_PATH . '/public/css/select2.min.css');
        $this->view->setVar('headerCss', $headerCss);

//        $text = Texts::findFirst(['textid = ?0 AND languages_id = ?1', 'bind' => ['page.about.title', 2], 'columns' => ['text']]);
//        $obj = ReObject::findFirstById(4004);

        $townsArr = ReTown::getSortedArray();
        $filterForm = new FilterForm((object)$this->request->get() ?: [], ['towns' => $townsArr]);

        array_unshift($townsArr, ['id' => 0, 'name' => $this->tx->query('menu.any'), 'region_id' => 0]);
        $this->view->setVar('townsList', json_encode($townsArr));

        $newObjects = ReObject::find([
            'conditions' => "active = 'Y'",
            'order' => 'created DESC',
            'limit' => '6'
        ]);
        $this->view->setVar('newObjects', $newObjects);

        $sortBy = 'default';
        $isFiltered = false;

        if ($this->request->isGet() && $this->request->get('fs',['trim', 'string','int!'])) {
            if (!$filterForm->isValid($this->request->get())) {
                $errorsArray = array();
                foreach ($filterForm->getMessages() AS $msg) {
                    $errorsArray[$msg->getField()] = $msg->getMessage();
                }
                if ($this->request->isAjax()) {
                    // print json_encode(array('status' => 'fail', 'messages' => $errorsArray, 'whoops' => 'yeaaah'));
                } else {
                    foreach ($errorsArray as $message) {
                        $this->flashSession->error($message);
                    }
                }
                $filterObjects = ReObject::find([
                    'conditions' => "istop = 'Y' AND active = 'Y'",
                    'order' => 'created DESC',
                    'limit' => '12'
                ]);
                $this->view->setVar('filterObjects', $filterObjects);
            } else {
                $isFiltered = true;

                $dealTypeId = $this->request->get('deal',['trim', 'string','int!']);
                $typeId = $this->request->get('type',['trim', 'string','int!']);
                $regionId = $this->request->get('region',['trim', 'string','int!']);
                $townId = $this->request->get('town',['trim', 'string','int!']);
                $minprice = $this->request->get('minprice',['trim', 'string','int!']);
                $maxprice = $this->request->get('maxprice',['trim', 'string','int!']);
                $objectId = $this->request->get('object',['trim', 'string','int!']);
                $isFirstline = $this->request->get('firstline',['trim', 'string','int!']);
                $sortBy = $this->request->get('sort',['trim', 'string']);

                $whereCondition = "active = 'Y'";
                $bindArr = [];
                $strQuery = '?fs=1';

                $filterTitlePrefix = $this->tx->query('list.all');
                $filterTitleSuffix = '';

                if ($objectId) {
                    $this->view->disable();
                    $this->response->redirect('/'.$this->session->get('lang').'/object/'.$objectId);
//                    $whereCondition .= ' AND id=:object_id:';
//                    $bindArr['object_id'] = $objectId;
//                    $strQuery .= '&object='.$objectId;
                } else {
                    if ($typeId) {
                        $whereCondition .= ' AND re_types_id=:type_id:';
                        $bindArr['type_id'] = $typeId;
                        $strQuery .= '&type='.$typeId;
                        $filterTitlePrefix = $this->tx->query('re_types.title.'.$typeId);
                    }

                    if ($dealTypeId) {
                        $whereCondition .= ' AND re_deal_type_id=:deal_type_id:';
                        $bindArr['deal_type_id'] = $dealTypeId;
                        $strQuery .= '&deal='.$dealTypeId;
                        $filterTitlePrefix .= ' '.$this->tx->query('re_deal_type.title.'.$dealTypeId);
                    }
                    if ($regionId) {
                        $townsByRegRaw = ReTown::find([
                            'columns' => 'id',
                            'conditions' => "re_region_id = ?1",
                            'bind'       => [1 => $regionId],
                        ])->toArray();
                        if ($townsByRegRaw && count($townsByRegRaw)) {
                            $townsByRegArr = array_map(function($v) { return $v['id']; }, $townsByRegRaw);
                            $whereCondition .= ' AND re_town_id IN (' . implode(', ', $townsByRegArr) . ')';
                        }
                        $strQuery .= '&region='.$regionId;
                        $regionObj = ReRegion::findFirstById($regionId);
                        if ($regionObj) {
                            $filterTitleSuffix = ' ('.$this->tx->query('region.'.$regionId, $regionObj->name);
                        }
                    }
                    if ($townId) {
                        $whereCondition .= ' AND re_town_id=:town_id:';
                        $bindArr['town_id'] = $townId;
                        $strQuery .= '&town='.$townId;
                        $townObj = ReTown::findFirstById($townId);
                        if ($townObj) {
                            $filterTitleSuffix .= ($filterTitleSuffix === '' ? ' (' : ' - ') . $this->tx->query('re_town.name.'.$townId, $townObj->name);
                        }
                    }

                    $ismin = ' minprice is not null and minprice <> 0.0 ';
                    $isbest = ' bestprice is not null and bestprice <> 0.0 ';
                    $isprice = " not(($isbest) or ($ismin)) ";

                    if ($minprice) {
                        $whereCondition .= " AND ( ( $ismin and  maxprice >= :minprice: ) OR ( $isbest and bestprice >= :minprice: ) OR ( $isprice AND price >= :minprice: ) )";
                        $bindArr['minprice'] = $minprice;
                        $strQuery .= '&minprice='.$minprice;
                    }
                    if ($maxprice) {
                        $whereCondition .= " AND ( ( $ismin and  minprice <= :maxprice:  ) OR ( $isbest and bestprice <= :maxprice: ) OR ( $isprice and price <= :maxprice: ) )";
                        $bindArr['maxprice'] = $maxprice;
                        $strQuery .= '&maxprice='.$maxprice;
                    }
                    if ($isFirstline) {
                        $whereCondition .= ' AND firstline=:firstline:';
                        $bindArr['firstline'] = 'Y';
                        $strQuery .= '&firstline=1';
                    }
                }

                if (in_array($sortBy, ['created','price'])) {
                    $this->session->set('sort', $sortBy);
                } elseif ($this->session->has('sort')) {
                    $sortBy = $this->session->get('sort');
                }

                switch ($sortBy) {
                    case 'created':
                        $order = 'created DESC';
                        $strQuery .= '&$sort='.$sortBy;
                        break;
                    case 'price':
                        $order = 'price_for_sorting ASC';
                        $strQuery .= '&$sort='.$sortBy;
                        break;
                    default:
                        $order = 'created DESC';
                }

                $filterObjects = ReObject::find([
                    'conditions' => $whereCondition,
                    'bind'       => $bindArr,
                    'order'      => $order,
                    //'limit' => '12'
                ]);
                $this->view->setVar('filterObjects', $filterObjects);
                $this->view->setVar('strQuery', $strQuery);

                $numberPage = $this->request->getQuery('page', 'int');
                $paginator = new PaginatorModel(array(
                    'data' => $filterObjects,
                    'limit' => 21, //100
                    'page' => $numberPage
                ));
                $this->view->page = $paginator->getPaginate();
                if ($filterTitleSuffix !== '') {
                    $filterTitleSuffix .= ')';
                }
                $this->view->setVar('filterTitle', $filterTitlePrefix . $filterTitleSuffix);
            }
        } else {
            $headerImages = HeaderImage::find(['conditions' => "languages_id=?1 AND active='Y'", 'bind' => [1 => $this->session->get('langId')]]);
            $this->view->setVar('headerImages', $headerImages);

            $filterObjects = ReObject::find([
                'conditions' => "istop = 'Y' AND active = 'Y'",
                'order' => 'created DESC',
                'limit' => '12'
            ]);
            $this->view->setVar('filterObjects', $filterObjects);
        }
        $this->view->setVar('isFiltered', $isFiltered);
        $this->view->setVar('sortBy', $sortBy);

        $this->view->filterForm = $filterForm;
    }
}

