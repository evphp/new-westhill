<?php

class ErrorController extends ControllerBase
{

    public function indexAction()
    {
        $this->dispatcher->forward(array('controller' => 'error', 'action' => 'pageNotFound'));
    }

    public function pageNotFoundAction()
    {

        $exception = $this->dispatcher->getParam('exception');

        $this->view->setVar('errorMessageForAdmin', $exception);

        //$this->view->pick('pageIsNotResponding');
    }

    public function pageIsNotRespondingAction()
    {
        if ($this->request->isAjax()) {
            if ($this->dispatcher->getParam('exception') !== null) {
                $exception = $this->dispatcher->getParam('exception');
                $this->view->setVar('errorMessageForAdmin', $exception);

                $errorMessage = '
                Exception: ' . $exception . ' <BR>
                Controller: ' . $this->dispatcher->getParam('controllerName') . '<BR>
                Action: ' . $this->dispatcher->getParam('actionName') . '<BR>
                file: ' . $exception->getFile() . '<BR>
                line: ' . $exception->getLine() . '<BR>
                trace: ' . $exception->getTraceAsString() . '<BR>
                ';
                //$this->notify->sendDebug(array("message" => $errorMessage));
            }
            print json_encode(array('status' => 'fail', 'messages' => $this->tx->query('Error occurred. Please tr again later'), 'errorMessage' => $errorMessage));
            $this->view->disable();
            $this->response->setContentType('application/json', 'UTF-8');
        } else {
            $exception = $this->dispatcher->getParam('exception');
            $this->view->setVar('errorMessageForAdmin', $exception);
        }
    }
}
