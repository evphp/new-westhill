<?php

//use Library\Pdf\Pdf;
use Components\Forms\ContactForm;
use Components\Forms\FilterForm;

class ObjectController extends ControllerBase
{
    public function indexAction($objectId = 0)
    {
        $object = ReObject::findFirstById($objectId);

        if (!$object) {
            $this->view->disable();
            $this->response->redirect('/');
        } else {
            try {
                $object->viewed++;
                $object->save();
            } catch (\Exception $exception) {}
        }
        Phalcon\Tag::setTitle($this->tx->query('re_types.title.'.$object->re_types_id));

        $headerCss[] = '/css/slick.css?v=' . filemtime(BASE_PATH . '/public/css/slick.css');
        $headerCss[] = '/css/slick-theme.css?v=' . filemtime(BASE_PATH . '/public/css/slick-theme.css');
        $headerCss[] = '/css/select2.min.css?v=' . filemtime(BASE_PATH . '/public/css/select2.min.css');
        $this->view->setVar('headerCss', $headerCss);

        $footerJs[] = '/js/slick.min.js?v=' . filemtime(BASE_PATH . '/public/js/slick.min.js');
        $footerJs[] = '/js/module/object.js?v=' . filemtime(BASE_PATH . '/public/js//module/object.js');
        $footerJs[] = 'https://www.google.com/recaptcha/api.js?onload=contactFormCaptchaCallbackV2&render=explicit';
        $this->view->setVar('footerJs', $footerJs);

        $this->view->setVar('object', $object);
        //$this->view->setVar('metaDescription', $this->tx->query('re_object.description.'.$object->id));
        $this->view->setVar('ogMetaTitle', $this->tx->query('re_object.name.'.$object->id));
        //$this->view->setVar('ogMetaDescription', $this->tx->query('re_object.description.'.$object->id));
        $this->view->setVar('navLinkParams', $object->id);

        $newObjects = ReObject::find([
            'conditions' => "active = 'Y'",
            'order' => 'created DESC',
            'limit' => '6'
        ]);
        $this->view->setVar('newObjects', $newObjects);

        $contactForm = new ContactForm();
        $this->view->contactForm = $contactForm;

        $townsArr = ReTown::getSortedArray();
        $filterForm = new FilterForm((object)$this->request->get() ?: [], ['towns' => $townsArr]);
        $this->view->filterForm = $filterForm;
//        $this->view->setVar('headerImages', false);
    }

//    public function printAction($objectId = 0)
//    {
//        $this->view->disable();
//
//        $object = ReObject::findFirstById($objectId);
//
//        if (!$object) {
//            $this->response->redirect('/');
//        }
//
//        $pdfObj = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//        //$pdfObj->test($objectId);
//        $pdfObj->printTest();
//        die;
//    }
}

