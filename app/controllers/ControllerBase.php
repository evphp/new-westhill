<?php

class ControllerBase extends \Phalcon\Mvc\Controller
{
	public function initialize()
    {
        $params = $this->dispatcher->getParams();
        /**
         * security headers
         * https://habr.com/ru/company/hosting-cafe/blog/315802/
         */
        $this->response->setHeader('X-Frame-Options', 'SAMEORIGIN');      // deny iframe for another one sites
        $this->response->setHeader('X-Content-Type-Options', 'nosniff');  // deny sniff server MIME type
//        $this->response->setHeader('Access-Control-Allow-Origin', '*'); // allow cross-domain ajax
//        $this->response->setHeader('Public-Key-Pins', 'pin-sha256="jWW......"; pin-sha256="klO....."; pin-sha256="kUF...."; max-age=15552000'); // deny MITM attack
//        $this->response->setHeader('Strict-Transport-Security', 'max-age=31536000; includeSubDomains'); // allow HTTPS only
        //$this->response->setHeader('X-XSS-Protection', '1; mode=block');  // deny xss attack (dont works with cloudflare)

        if (isset($params['language']) && !in_array($params['language'], (array)$this->config->application->availableLanguages, true)) {
            $uriStr = $this->request->getURI();
            $uriStr = str_replace('/'.$params['language'].'/', '/'.$this->config->application->defaultLang.'/', $uriStr);
            $this->view->disable();
            $this->response->redirect($uriStr);
        }

        $lang = (isset($params['language']) && in_array($params['language'], (array)$this->config->application->availableLanguages, true)) ? $params['language'] : ($this->session->get('lang') ?: $this->config->application->defaultLang);
        if ($this->session->get('lang') !== $lang) {
            $this->session->set('lang', $lang);
            $this->session->set('langId', $this->config->application->enabledLanguages[$lang]['id']);
        }
        $this->view->setVar('filePrefix', $this->config->application->filePrefix);

        $stylesVer = filemtime(BASE_PATH . '/public/css/style.css');
        $mainJSVer = filemtime(BASE_PATH . '/public/js/main.js');

        $this->view->setVar('stylesVer', $stylesVer);
        $this->view->setVar('mainJSVer', $mainJSVer);

//        $this->view->setVar('metaDescription', $this->tx->query('re_types.title.'.$object->re_types_id));
//        $this->view->setVar('ogMetaTitle', $this->tx->query('re_object.name.'.$object->id));
//        $this->view->setVar('ogMetaDescription', $this->tx->query('re_object.description.'.$object->id));
	}
}