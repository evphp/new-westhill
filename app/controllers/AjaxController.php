<?php

use Components\Forms\ContactForm;
use Library\Google\GoogleRecaptcha2;

class AjaxController extends ControllerBase
{
	public function contactAction()
    {
        $this->view->disable();

        $postForm = new ContactForm();

        if ($this->request->isPost() && $this->request->isAjax()) {

            $isHuman = GoogleRecaptcha2::isHuman($this->request->getPost('g-recaptcha-response'));

            if (!$isHuman) {
                if ($this->request->isAjax()) {
                    $this->response->setJsonContent(['status' => 'fail', 'isCaptchaReset' => true, 'messages' => array('email' => $this->tx->query('Recaptcha missing input.'))]);
                    $this->response->setContentType('application/json', 'UTF-8');
//                } else {
//                    $this->flashSession->error('Recaptcha missing input.');
//                    $this->response->redirect('');
                }
                return $this->response;
            }
            if ($this->security->checkToken()) {
                if (!$postForm->isValid($this->request->getPost())) {
                    $errorsArray = array();
                    foreach ($postForm->getMessages() as $msg) {
                        $errorsArray[$msg->getField()] = $msg->getMessage();
                    }
                    $this->response->setJsonContent(array(
                        'status' => 'fail',
                        'messages' => $errorsArray,
                        'tokenKey' => $this->security->getTokenKey(),
                        'token' => $this->security->getToken(),
                        'isCaptchaReset' => true
                    ));
                } else {

                    $name = $this->request->getPost('cfName', ['trim','string']);
                    $email = $this->request->getPost('cfEmail', ['trim','string']);
                    $message = $this->request->getPost('cfMessage', ['trim','string']);
                    $objecId = $this->request->getPost('object', ['int!']);

                    try {
                        $this->notify->sendEmail($this->config->mail->fromEmail, $this->tx->query('Request from website'), array(
                            'ajax',
                            'contact'
                        ), array(), array('from' => $name, 'email' => $email, 'message' => $message, 'object' => $objecId));
                    } catch (\Swift_TransportException $e) {
                        //print_r($e);
                    }
                    $this->response->setJsonContent(array('status' => 'success', 'isMessage' => true, 'message' => $this->tx->query('Message is sent'), 'url' => ''));
                }
            } else {
                $this->response->setJsonContent(array(
                    'status' => 'fail',
                    'messages' => '',
                    'url' => '/'
                ));
            }
            $this->response->setContentType('application/json', 'UTF-8');
            return $this->response;
        } else {
            $this->response->redirect('/');
        }
    }

	public function sendFeedbackAction()
    {
		if($this->request->isPost()) {

			$email   = $this->request->getPost( 'cfEmail' );
			$subject = $this->request->getPost( 'cfSubject' );
			$message = $this->request->getPost( 'cfMessage' );

			if ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
				$rmessage = array(
					'status'   => 'fail',
//					'messages' => array( 'cfEmail' => $this->translate->query( "E-mail is not valid" ) )
                    'messages' => array( 'cfEmail' => "E-mail is not valid" )
				);
			} else {
				if ( mb_strlen( $subject ) <= 2 ) {
					$rmessage = array(
						'status'   => 'fail',
						'messages' => array( 'cfSubject' => "Subject is too short" )
					);
				} else {
					if ( mb_strlen( $message ) <= 2 ) {
						$rmessage = array(
							'status'   => 'fail',
							'messages' => array( 'cfMessage' => "Message is too short" )
						);
					} else {

						try {
							$this->notify->sendEmail( $this->config->mail->fromEmail, "Request from website", array(
                                'ajax',
                                'contactFrom'
                            ), array(), array( 'subject' => $subject, 'email' => $email, 'message' => $message ) );

						} catch ( \Swift_TransportException $e ) {
							print_r( $e );
						}

						$rmessage = array(
							'status'    => 'success',
							'isMessage' => true,
							'message'   => "Message is sent",
							'url'       => ''
						);
					}
				}
			}
			print json_encode( $rmessage );
		}

        $this->view->disable();
        $this->response->setContentType('application/json', 'UTF-8');
	}

}

