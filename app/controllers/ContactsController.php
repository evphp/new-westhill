<?php

use Components\Forms\ContactForm;

class ContactsController extends ControllerBase
{
    public function indexAction()
    {
        Phalcon\Tag::setTitle($this->tx->query("page.contacts.title") );

        $footerJs[] = 'https://www.google.com/recaptcha/api.js?onload=contactFormCaptchaCallbackV2&render=explicit';
        $this->view->setVar('footerJs', $footerJs);

        $contactForm = new ContactForm();
        $this->view->contactForm = $contactForm;
    }
}

